﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Megaroboticon
{
    /// <summary>
    /// A child of the GUI class.
    /// The ObjWindow is the Objective Window and serves as a player aid, giving them a hint as to what they must do to progress to the next stage of the level.
    /// A list of objectives for each level is stored here.
    /// It also handles 'help' text which appears at certain points where the player has to press an unusual button to ineract with the something (e.g. a terminal or elevator).
    /// </summary>
   
    class ObjWindow : GUI
    {
        // CLASS VARIABLES
        // Current Level Ref
        private Game1.PlayLevel m_currPlayLevel;

        // Objective arrays (by level) & Selector
        private String[] LV0_Objectives;
        private String[] LV1_Objectives;
        private String[] LV2_Objectives;
        private String[] LV3_Objectives;
        private int m_currObj;

        // Position
        private Vector2 m_newPos;

        // Display Font
        private SpriteFont m_objFont;

        // Font Offset
        public static int objOffset_x = 5;
        public static int objOffset_y = 20;
        public static int helpOffset_x = 5;
        public static int helpOffset_y = -550;
     
        // CONSTRUCTOR
        public ObjWindow(Texture2D txr, Vector2 pos, int screenWidth, int screenHeight, SpriteFont objFont)
            : base(txr, pos, screenWidth, screenHeight)
        {
            m_objFont = objFont;
            m_currPlayLevel = Game1.PlayLevel.LV0;
            m_currObj = 0;
            
            // Initialise and populate level objective arrays:
            // Level 0
            LV0_Objectives = new String[2];
            LV0_Objectives[0] = "Show your ID at reception.";
            LV0_Objectives[1] = "Go to Elevator.";
            // Level 1
            LV1_Objectives = new String[2];
            LV1_Objectives[0] = "Unlock Elevator.";
            LV1_Objectives[1] = "Go to Elevator.";
            // Level 2
            LV2_Objectives = new String[3];
            LV2_Objectives[0] = "Open Security Doors.";
            LV2_Objectives[1] = "Unlock Elevator.";
            LV2_Objectives[2] = "Go to Elevator.";
            // Level 4
            LV3_Objectives = new String[4];
            LV3_Objectives[0] = "Open Security Doors.";
            LV3_Objectives[1] = "Unlock Elevator.";
            LV3_Objectives[2] = "Disarm Floor Alarm.";
            LV3_Objectives[3] = "Go to Elevator.";
        }

        // UPDATE
        public void updateme(Vector2 cam_Pos)
        {
            // Update and set new position
            m_newPos = new Vector2(cam_Pos.X + 150, cam_Pos.Y + (ScreenHeight - 110));
            setPosition(m_newPos);

        }

        // OBJECTIVE COMPLETE
        public void objectiveCompleted()
        {
            // Increase objective selector
            m_currObj++;
        }

        // RESET OBJECTIVES
        public void resetObjective(Game1.PlayLevel currPlayLevel)
        {
            // Set Current Play Level & Reset Objective Selector
            m_currPlayLevel = currPlayLevel;
            m_currObj = 0;
        }

        // DRAW : Display
        public void drawdisplay(SpriteBatch sb)
        {
            // Draw the the selected (current) objective from the level array
            switch (m_currPlayLevel)
            {
                case Game1.PlayLevel.LV0:
                    {
                        sb.DrawString(m_objFont, "" + LV0_Objectives[m_currObj], new Vector2(m_newPos.X + objOffset_x, m_newPos.Y + objOffset_y), Color.Cyan);
                        break; 
                    }
                case Game1.PlayLevel.LV1:
                    {
                        sb.DrawString(m_objFont, "" + LV1_Objectives[m_currObj], new Vector2(m_newPos.X + objOffset_x, m_newPos.Y + objOffset_y), Color.Cyan);
                        break; 
                    }
                case Game1.PlayLevel.LV2:
                    {
                        sb.DrawString(m_objFont, "" + LV2_Objectives[m_currObj], new Vector2(m_newPos.X + objOffset_x, m_newPos.Y + objOffset_y), Color.Cyan);
                        break; 
                    }
                case Game1.PlayLevel.LV3:
                    {
                        sb.DrawString(m_objFont, "" + LV3_Objectives[m_currObj], new Vector2(m_newPos.X + objOffset_x, m_newPos.Y + objOffset_y), Color.Cyan);
                        break; 
                    }
                case Game1.PlayLevel.LV4:
                    {
                        sb.DrawString(m_objFont, "Access Director's Terminal.", new Vector2(m_newPos.X + objOffset_x, m_newPos.Y + objOffset_y), Color.Cyan);
                        break; 
                    }
            }   
        }

        // DRAW : Terminal Help
        public void drawterminalhelp(SpriteBatch sb)
        {
            // For Level 0 draw button prompt to talk to receptionist
            if (m_currPlayLevel == Game1.PlayLevel.LV0)
            {
                sb.DrawString(m_objFont, "Press Right Shoulder Button to show the receptionist your ID.", new Vector2(m_newPos.X + helpOffset_x, m_newPos.Y + helpOffset_y), Color.White);
            }
            // For all other levels draw button prompt to access terminal
            else
            {
                sb.DrawString(m_objFont, "Press Right Shoulder Button to access terminal.", new Vector2(m_newPos.X + helpOffset_x, m_newPos.Y + helpOffset_y), Color.White);
            }
        }

        // DRAW : Elevator Help
        public void drawelevatorhelp(SpriteBatch sb)
        {
            // Draw button prompt to get in the elevator
            sb.DrawString(m_objFont, "Press Right Shoulder Button to get in the elevator.", new Vector2(m_newPos.X + helpOffset_x, m_newPos.Y + helpOffset_y), Color.White);
 
        }

        // DRAW : Pause Help
        public void drawpausehelp(SpriteBatch sb)
        {
            // Draw button prompts to resume play or quit.
            sb.DrawString(m_objFont, "Press Start to Resume.", new Vector2(m_newPos.X + helpOffset_x, m_newPos.Y + helpOffset_y), Color.Cyan);
            sb.DrawString(m_objFont, "Press Back to Quit.", new Vector2(m_newPos.X + helpOffset_x, m_newPos.Y + helpOffset_y + 30), Color.Cyan);

        }

        // DRAW : Loss Screen Help
        public void drawlosshelp(SpriteBatch sb, String failmessage)
        {
            // Draw failure message & button prompts to restart level or quit.
            sb.DrawString(m_objFont, ""+failmessage, new Vector2(m_newPos.X + helpOffset_x, m_newPos.Y + helpOffset_y), Color.Red);
            sb.DrawString(m_objFont, "Press Start to Restart.", new Vector2(m_newPos.X + helpOffset_x, m_newPos.Y + helpOffset_y + 30), Color.Red);
            sb.DrawString(m_objFont, "Press Back to Quit.", new Vector2(m_newPos.X + helpOffset_x, m_newPos.Y + helpOffset_y + 60), Color.Red);
        }

    }
}
