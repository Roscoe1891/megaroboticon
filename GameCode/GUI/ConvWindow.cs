﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Megaroboticon
{
    /// <summary>
    /// The conversation window is a child of the GUI class.
    /// It is used to contain text 'spoken' by NPC's, which is shown to the player one line at a time.
    /// At present there is only one of these (the receptionist on the ground floor).
    /// </summary>
    class ConvWindow : GUI
    {
        // CLASS VARIABLES

        // Array & selector to store the receptionist's phrases
        private String[] m_receptionPhrases;
        private int m_phraseNumber;

        // Fonts
        private SpriteFont m_convFont;
        private SpriteFont m_convFontSmall;

        // Determine when the conversation is over
        private bool m_convOver;
        public bool ConvOver
        {
            get
                { return m_convOver; }
        }

        // CONSTRUCTOR
        public ConvWindow(Texture2D txr, Vector2 pos, int screenWidth, int screenHeight, string playername, SpriteFont convFont, SpriteFont convFontSmall)
            : base(txr, pos, screenWidth, screenHeight)
        {
            // Initialise and populate the receptionist's phrase list & selector
            m_receptionPhrases = new String[6];
            m_receptionPhrases[0] = "Hi there. Just a moment while I check your ID.";
            m_receptionPhrases[1] = "...";
            m_receptionPhrases[2] = "" +playername +", huh?  Here about the IT problem...";
            m_receptionPhrases[3] = "I believe that was up on 12th.";
            m_receptionPhrases[4] = "Most of them up there will have gone home by now...";
            m_receptionPhrases[5] = "But I can open the elevator for you. Head on up.";

            m_phraseNumber = 0;

            // Fonts
            m_convFont = convFont;
            m_convFontSmall = convFontSmall;

            // Default conversation over to false
            m_convOver = false;
        }

        // UPDATE
        public void updateme(GamePadState padCurr, GamePadState padOld, Map currMap)
        {
            // If left shoulder button is pressed
            if (padCurr.Buttons.LeftShoulder == ButtonState.Pressed && padOld.Buttons.LeftShoulder != ButtonState.Pressed)
            {
                // Increase the phrase number
                if (m_phraseNumber < m_receptionPhrases.Length)
                {
                    m_phraseNumber++;
                }

                // If the last phrase is showing, open the elevator and end the conversation
                if (m_phraseNumber == m_receptionPhrases.Length)
                {
                    currMap.openElevator();
                    m_convOver = true;
                }
            }
        }

        // Draw the display text
        public void drawdisplay(SpriteBatch sb, Vector2 cam_Pos)
        {
            // Current Receptionist Phrase
            if (m_phraseNumber <= m_receptionPhrases.Length - 1)
            {
                sb.DrawString(m_convFont, "Receptionist:", new Vector2(cam_Pos.X + 155, cam_Pos.Y + 600), Color.Cyan);
                sb.DrawString(m_convFont, m_receptionPhrases[m_phraseNumber], new Vector2(cam_Pos.X + 155, cam_Pos.Y + 640), Color.Cyan);
            }

            // Button Prompt
            sb.DrawString(m_convFontSmall, ">> Press Left Shoulder Button", new Vector2(cam_Pos.X + 580, cam_Pos.Y + 670), Color.Cyan);
        }
    }
}
