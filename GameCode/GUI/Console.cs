﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;


namespace Megaroboticon
{
    /// <summary>
    /// The Console is a child of the GUI class.
    /// It is used by the player to hack terminals to allow them to progress through the level.
    /// 
    /// 'Hack' attempts using the console go under the following sequence:
    /// 1) The console obtains a randomised key code.
    /// 2) The console shows this to the player.
    /// 3) When the player is ready to enter to the key code, they have a limited time to input it from memory.
    /// 4) If the player does not enter the right code in time, or enters the wrong code, the hack fails and this trigger the alarm (bad news for the player if there are enemies nearby)
    /// 5) If the player enters the correct code, they are presented with one or more 'security procedures' which will allow them to move to the next stage of the level, or on to the next level.
    /// 6) Once the player has run all of the security procedures, the hack is counted as a success.
    /// 
    /// The console is also used as a 'text box' to show the introduction and victory text.
    /// </summary>

    // Enumerator type to define Console states
    public enum ConsoleState { GetKey, GotKey, EnterKey, EnterCmd }
    class Console : GUI
    {
        // Map & Level references
        private Map m_currMap;
        private Game1.PlayLevel m_currPlayLevel;

        // Console Position
        private Vector2 m_currPos;
        
        // Time bars and timers
        private Texture2D m_timeBarTxr;
        private static int m_TimeBarMaxWidth = 475;
        private int m_securityKeyTimer;
        private int m_securityKeyTime;
        private Rectangle m_securityKeyTimeBar;
        private Rectangle m_KeyEntryTimeBar;
        private int m_keyEntryTimer;
        private int m_keyEntryTime;

        // Current console state
        private ConsoleState m_currentState;

        // Terminal Number generator array
        private int[] m_terminalNumber;
        // Terminal Key Code generator array
        private int[] m_terminalKey;
        // Entered Key Code array & selector
        private int[] m_enteredKey;
        private int m_selectedKeyNum;
        // List of security procedures associated with terminal, and selector.
        private List<String> m_securityProcs;
        private int m_selectedProc;
        // Determine whether correct key code was entered
        private bool m_correctKeyEntered;

        // Fonts & text lists
        private SpriteFont m_displayFont;
        private SpriteFont m_displayFontSmall;
        private List<String> m_introText;
        private List<String> m_winText;

        // Determine whether the hack attempt was successful
        private bool m_hackSuccess;
        public bool HackSuccess
        {
            get { return m_hackSuccess; } 
        }

        // Determine whether the hack attempt was a failure
        private bool m_hackFail;
        public bool HackFail
        {
            get { return m_hackFail; }
        }
      
        // CONSTRUCTOR
        public Console(Texture2D txr, Vector2 pos, int screenWidth, int screenHeight, SpriteFont displayFont, SpriteFont displayFontSmall, Texture2D timeBarTxr, Map currMap)
            : base(txr, pos, screenWidth, screenHeight)
        {
            //  Map & level references
            m_currMap = currMap;
            m_currPlayLevel = Game1.PlayLevel.LV0;
            
            // Position
            m_currPos = pos;

            // Fonts
            m_displayFont = displayFont;
            m_displayFontSmall = displayFontSmall;
            
            // Time bar & timers
            m_timeBarTxr = timeBarTxr;
            m_securityKeyTime = 120;
            m_securityKeyTimer = 0;
            m_securityKeyTimeBar = new Rectangle(0, 0, timeBarTxr.Width, timeBarTxr.Height);
            m_keyEntryTime = 475;
            m_keyEntryTimer = m_keyEntryTime;
            m_KeyEntryTimeBar = new Rectangle(0, 0, timeBarTxr.Width, timeBarTxr.Height);

            // Initial state
            m_currentState = ConsoleState.GetKey;

            // Generate a random terminal number
            m_terminalNumber = new int[8];
            for (int i = 0; i < m_terminalNumber.Length; i++)
            {
                m_terminalNumber[i] = Game1.RNG.Next(0, 9);
            }

            // Generate a random terminal key
            m_terminalKey = new int[4];
            for (int i = 0; i < m_terminalKey.Length; i++)
            {
                m_terminalKey[i] = Game1.RNG.Next(1,9);
            }

            // Generate a "blank" entered key & selector for the player to input to
            m_enteredKey = new int[4];
            for (int i = 0; i < m_enteredKey.Length; i++)
            {
                m_enteredKey[i] = 0;
            }
            m_selectedKeyNum = 0;

            // Initialise security procedures list and selector
            m_securityProcs = new List<String>();
            m_selectedProc = 0;

            // Initialise introductory text and victory text.
            m_introText = new List<String>();
            m_winText = new List<String>();
       
            // Default boolean values to false
            m_correctKeyEntered = false;
            m_hackSuccess = false;
            m_hackFail = false;
        }

        // Reset the console
        public void resetConsole()
        {
            // Reset security key timer
            m_securityKeyTimer = 0;
            m_keyEntryTimer = m_keyEntryTime;

            // Reset Console state
            m_currentState = ConsoleState.GetKey;

            // Generate new random terminal number
            m_terminalNumber = new int[8];
            for (int i = 0; i < m_terminalNumber.Length; i++)
            {
                m_terminalNumber[i] = Game1.RNG.Next(0, 9);
            }

            // Generate new terminal key code
            m_terminalKey = new int[4];
            for (int i = 0; i < m_terminalKey.Length; i++)
            {
                m_terminalKey[i] = Game1.RNG.Next(1, 9);
            }

            // Blank the key entry array & selector
            m_enteredKey = new int[4];
            for (int i = 0; i < m_enteredKey.Length; i++)
            {
                m_enteredKey[i] = 0;
            }
            m_selectedKeyNum = 0;

            // Initialise a new list of security procedures & selector
            m_securityProcs = new List<String>();
            m_selectedProc = 0;

            // Reset boolean values to false
            m_correctKeyEntered = false;
            m_hackSuccess = false;
            m_hackFail = false;

            // Clear the intro & victory text lists
            m_introText.Clear();
            m_winText.Clear();
        }

        // Set the position of the console & display elements
        public void setConsole(Vector2 cam_Pos, Game1.PlayLevel currPlayLevel, Map currMap, int hackedTerminals)
        {
            // Set position of the console
            m_currPos = new Vector2(cam_Pos.X + ((ScreenWidth - Width) / 2), cam_Pos.Y + ((ScreenHeight - Height) / 2));
            setPosition(m_currPos);

            // Current Level Reference
            m_currPlayLevel = currPlayLevel;
            m_currMap = currMap;

            // Set position of the security key timer bar
            m_securityKeyTimeBar.X = (int)cam_Pos.X + 410;
            m_securityKeyTimeBar.Y = (int)cam_Pos.Y + 400;

            // Set position of the key entry timer bar
            m_KeyEntryTimeBar.X = (int)cam_Pos.X + 410;
            m_KeyEntryTimeBar.Y = (int)cam_Pos.Y + 420;

            #region SET SECURITY PROCEDURES
            switch (m_currPlayLevel)
            {
                // Add to the security procedures list.
                // (Dependant on the current level, and the progress as defined the number of terminals that have been hacked)
                // Level 1 Security Procs
                case Game1.PlayLevel.LV1:
                    {
                        m_securityProcs.Add("Unlock Elevator (Floors 13-40)");
                        break;
                    }
                // Level 2 Security Procs
                case Game1.PlayLevel.LV2:
                    {
                        if (hackedTerminals == 0)
                        {
                            m_securityProcs.Add("Security Door Override");
                        }
                        else if (hackedTerminals == 1)
                        {
                            m_securityProcs.Add("Unlock Elevator (Floors 41-60)");
                        }
                        break;
                    }
                // Level 3 Security Procs
                case Game1.PlayLevel.LV3:
                    {
                        if (hackedTerminals == 0)
                        {
                            m_securityProcs.Add("Security Door Override");
                        }
                        else if (hackedTerminals == 1)
                        {
                            m_securityProcs.Add("Unlock Elevator (Floors 61-75)");
                        }
                        else if (hackedTerminals == 2)
                        {
                            m_securityProcs.Add("Emergency Shutdown");
                        }
                        break;
                    }
                // Level 4 Security Procs
                case Game1.PlayLevel.LV4:
                    {
                        m_securityProcs.Add("Download Restricted File: Project Aasimov");
                        m_securityProcs.Add("Download Restricted File: Project Gibson");
                        m_securityProcs.Add("Download Restricted File: Project Ballard");
                        break;
                    }
            }
            #endregion
        }

        // Set the introduction text (which requires the player's name)
        public void setintrotext(String playername)
        {
            m_introText.Add("Megaroboticon is the world's largest synthetic personnel supplier.");
            m_introText.Add("We believe they are hiding dark secrets from the public.");
            m_introText.Add("That's why we need you, " +playername +".");
            m_introText.Add("We need a hacker with your skills to infiltrate their headquarters.");
            m_introText.Add("You must get to the Director's office on the top floor.");
            m_introText.Add("Hack the Director's terminal and download the project files.");
            m_introText.Add("Don't get caught by the guards and try not to let them see you.");
            m_introText.Add("The meter to the left of your screen shows their alert level.");
            m_introText.Add("If it reaches full, the guards will become hostile. ");
            m_introText.Add("You have a fake ID to get you into the building.");
            m_introText.Add("After that you will have to hack the terminals to bypass security.");
            m_introText.Add("Press Start to begin...");
        }

        // Set the victory text (which requires the player's name and final score)
        public void setwintext(String playername, double playerScore)
        {
            m_winText.Add("Excellent work, " +playername +"!");
            m_winText.Add("You got the project files!");
            m_winText.Add("Now the world will know the truth about Megaroboticon...");
            m_winText.Add("You have earned " +playerScore +" points.");
            m_winText.Add("You can view the High Score list from the Start Menu.");
            m_winText.Add("Press Start to return to the Start Menu.");
        }

        // UPDATE
        public void updateme(Vector2 cam_Pos, GamePadState currPad, GamePadState oldPad, Level currLevel, Map currMap)
        {
            // Console behaviour is dependant on current console state
            switch (m_currentState)
            {
                // GET KEY State
                // Shows progress timer for getting the security key 
                case ConsoleState.GetKey:
                    {
                        // Progress bar goes up
                        if (m_securityKeyTimer < m_securityKeyTime)
                        {
                            m_securityKeyTimer++;
                            m_securityKeyTimeBar.Width = m_securityKeyTimer * (m_TimeBarMaxWidth / m_securityKeyTime);
                        }

                        // When progress bar is full, switch state to "got key" 
                        if (m_securityKeyTimer >= m_securityKeyTime)
                        {
                            m_currentState = ConsoleState.GotKey;
                        }
                        break;
                    }
                // GOT KEY State
                // Shows the security key
                case ConsoleState.GotKey:
                    {
                        // Press Left Shoulder to switch state to "Enter key"
                        if (currPad.Buttons.LeftShoulder == ButtonState.Pressed)
                        {
                            m_currentState = ConsoleState.EnterKey;
                        }
                        break;
                    }
                // ENTER KEY State
                // Allows key entry
                case ConsoleState.EnterKey:
                    {
                        // Allow Key Entry using the Dpad
                        // Left or right to select number place
                        if ((currPad.DPad.Right == ButtonState.Pressed) && (oldPad.DPad.Right != ButtonState.Pressed))
                        {
                            if (m_selectedKeyNum < m_enteredKey.Length - 1)
                                m_selectedKeyNum++;
                        }
                        if ((currPad.DPad.Left == ButtonState.Pressed) && (oldPad.DPad.Left != ButtonState.Pressed))
                        {
                            if (m_selectedKeyNum > 0)
                                m_selectedKeyNum--;
                        }

                        // Up or down to select individual number
                        if ((currPad.DPad.Up == ButtonState.Pressed) && (oldPad.DPad.Up != ButtonState.Pressed))
                        {
                            if (m_enteredKey[m_selectedKeyNum] < 9)
                                m_enteredKey[m_selectedKeyNum]++;
                        }

                        if ((currPad.DPad.Down == ButtonState.Pressed) && (oldPad.DPad.Down != ButtonState.Pressed))
                        {
                            if (m_enteredKey[m_selectedKeyNum] > 0)
                                m_enteredKey[m_selectedKeyNum]--;
                        }

                        // Timer goes down
                        if (m_keyEntryTimer > 0)
                        {
                            m_keyEntryTimer--;
                            m_KeyEntryTimeBar.Width = m_keyEntryTimer * (m_TimeBarMaxWidth / m_keyEntryTime);
                        }

                        // If timer reaches 0, the hack has failed
                        if (m_keyEntryTimer == 0)
                            m_hackFail = true;

                        // Has the correct key been entered?
                        for (int i = 0; i < m_enteredKey.Length; i++)
                        {
                            m_correctKeyEntered = false;
                            if (m_enteredKey[i] == m_terminalKey[i])
                                m_correctKeyEntered = true;
                        }

                        // Left Shoulder Button triggers key entry
                        if ((currPad.Buttons.LeftShoulder == ButtonState.Pressed) && (oldPad.Buttons.LeftShoulder != ButtonState.Pressed))
                        {
                            // If the key entered was correct, switch to "Enter Command" state 
                            if (m_correctKeyEntered == true)
                            {
                                m_currentState = ConsoleState.EnterCmd;
                            }
                            // If the key was incorrect, hack fails
                            else { m_hackFail = true; }
                        }
                        break;
                    }
                // ENTER COMMAND State
                // Allows the player to run the selected security procedure
                // Executing a command removes it from the list of security procedures
                case ConsoleState.EnterCmd:
                    {
                        // Command execution triggered by Left Shoulder Button
                        if ((currPad.Buttons.LeftShoulder == ButtonState.Pressed) && (oldPad.Buttons.LeftShoulder != ButtonState.Pressed))
                        {
                            // If the selected proecudure relates to elevators, open the elevators on the level map
                            if (m_securityProcs[m_selectedProc].Contains("Elevator"))
                            {
                                m_currMap.openElevator();
                                m_securityProcs.RemoveAt(m_selectedProc);
                                m_selectedProc = 0;
                            }
                            // If the selected procedure relates to doors, change the doors on the level map
                            // (changing open doors to closed and closed doors to open)
                            else if (m_securityProcs[m_selectedProc].Contains("Door"))
                            {
                                m_currMap.securityDoors(m_currPlayLevel);
                                m_securityProcs.RemoveAt(m_selectedProc);
                                m_selectedProc = 0;
                            }
                            // If the selected procedure relates to a shutdown, shutdown the sentries on this level and deactivate alarmed tiles on the level map
                            else if (m_securityProcs[m_selectedProc].Contains("Shutdown"))
                            {
                                currLevel.securityshutdown();
                                currMap.removeAlarm();
                                m_securityProcs.RemoveAt(m_selectedProc);
                                m_selectedProc = 0;
                            }
                            // If the selected procedure relates to projects (no special function required as this ends the game)
                            else if (m_securityProcs[m_selectedProc].Contains("Project"))
                            {
                                m_securityProcs.RemoveAt(m_selectedProc);
                                m_selectedProc = 0;
                            }
                        }

                        // If the player has run all of the security procedures on the terminal, the hack was successful
                        if (m_securityProcs.Count == 0)
                        {
                            m_hackSuccess = true;
                        }
                        break;
                    }
            }
        }

        // Draw the console display
        public void drawdisplay(SpriteBatch sb, Vector2 cam_Pos)
        {
            switch (m_currentState)
            {
                // In the Get Key state
                case ConsoleState.GetKey:
                    {
                        // Draw terminal number, host user ID, time bar 
                        sb.DrawString(m_displayFont, "Accessed Megaroboticon Terminal:", new Vector2(cam_Pos.X + 410, cam_Pos.Y + 250), Color.Cyan);
                        sb.DrawString(m_displayFont, ">> M.0" + Math.Abs(cam_Pos.X) + "." + Math.Abs(cam_Pos.Y) + ".01", new Vector2(cam_Pos.X + 410, cam_Pos.Y + 270), Color.Cyan);
                        sb.DrawString(m_displayFont, "Hosted user ID :", new Vector2(cam_Pos.X + 410, cam_Pos.Y + 320), Color.Cyan);
                        for (int i = 0; i < m_terminalNumber.Length; i++)
                        {
                            sb.DrawString(m_displayFont, "" + m_terminalNumber[i], new Vector2(cam_Pos.X + 600 + (i * 10), cam_Pos.Y + 320), Color.Cyan);
                        }
                        sb.DrawString(m_displayFont, "Obtaining Security Administration Key:", new Vector2(cam_Pos.X + 410, cam_Pos.Y + 370), Color.Cyan);
                        sb.Draw(m_timeBarTxr, m_securityKeyTimeBar, Color.White);
                        break;
                    }
                // In the Got Key state
                case ConsoleState.GotKey:
                    {
                        // Draw the administrator key and button prompt
                        sb.DrawString(m_displayFont, "Obtained Security Administrator Key:", new Vector2(cam_Pos.X + 410, cam_Pos.Y + 300), Color.Cyan);
                        for (int i = 0; i < m_terminalKey.Length; i++)
                        {
                            sb.DrawString(m_displayFont, "" + m_terminalKey[i], new Vector2(cam_Pos.X + 600 + (i * 10), cam_Pos.Y + 330), Color.Cyan);
                        }
                        sb.DrawString(m_displayFont, "Press Left Shoulder Button to input key.", new Vector2(cam_Pos.X + 410, cam_Pos.Y + 390), Color.Cyan);
                        break;
                    }
                // In the Enter Key State
                case ConsoleState.EnterKey:
                    {
                        // Draw the button prompt, key entry and timer
                        sb.DrawString(m_displayFont, "Use the Dpad to input Key:", new Vector2(cam_Pos.X + 410, cam_Pos.Y + 300), Color.Cyan);
                        for (int i = 0; i < m_enteredKey.Length; i++)
                        {
                            sb.DrawString(m_displayFont, "" + m_enteredKey[i], new Vector2(cam_Pos.X + 600 + (i * 10), cam_Pos.Y + 330), Color.Cyan);
                        }
                        sb.DrawString(m_displayFont, "_", new Vector2(cam_Pos.X + 600 + (m_selectedKeyNum * 10), cam_Pos.Y + 331), Color.Cyan);
                        sb.Draw(m_timeBarTxr, m_KeyEntryTimeBar, Color.White);
                        sb.DrawString(m_displayFont, "Press Left Shoulder Button to enter key.", new Vector2(cam_Pos.X + 410, cam_Pos.Y + 390), Color.Cyan);
                        break;
                    }
                // In the Enter Command State
                case ConsoleState.EnterCmd:
                    {
                        // Draw the security procedures & button prompt
                        sb.DrawString(m_displayFont, "Execute Administrator Protocol:", new Vector2(cam_Pos.X + 410, cam_Pos.Y + 330), Color.Cyan);
                        for (int i = 0; i < m_securityProcs.Count; i++)
                        {
                            sb.DrawString(m_displayFont, " " + m_securityProcs[i], new Vector2(cam_Pos.X + 412 , cam_Pos.Y + 370 + i * 20), Color.Cyan);
                        }
                        sb.DrawString(m_displayFont, "> ", new Vector2(cam_Pos.X + 405, cam_Pos.Y + 370 + m_selectedProc * 20), Color.Cyan);
                        sb.DrawString(m_displayFont, "Press Left Shoulder Button to confirm.", new Vector2(cam_Pos.X + 410, cam_Pos.Y + 450), Color.Cyan);
                        break;
                    }
            }
        }

        // Draw the introduction display
        public void drawintrodisplay(SpriteBatch sb, Vector2 cam_Pos)
        {
            for (int i = 0; i < m_introText.Count; i++)
            {
                sb.DrawString(m_displayFontSmall, m_introText[i], new Vector2(cam_Pos.X + 410, cam_Pos.Y + 200 + i * 25), Color.Cyan);
            }
        }

        // Draw the victory display
        public void drawwindisplay(SpriteBatch sb, Vector2 cam_Pos)
        {
            for (int i = 0; i < m_winText.Count; i++)
            {
                sb.DrawString(m_displayFontSmall, m_winText[i], new Vector2(cam_Pos.X + 410, cam_Pos.Y + 200 + i * 30), Color.Cyan);
            }
        }


        
    }
}
