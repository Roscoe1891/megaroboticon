﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Megaroboticon
{
    /// <summary>
    /// The Fade Mask is used to fade in and out between certain scenes.
    /// </summary>
    class FadeMask
    {
        // CLASS VARIABLES
        // Alpha value 
        private int m_alpha;
        public int Alpha
        {
            get { return m_alpha; }
        }

        // Fade speed
        private int m_fadeSpeed;

        // Position & texture
        private Vector2 m_position;
        private Texture2D m_txr;
        
        // CONSTRUCTOR
        public FadeMask(Texture2D txr, Vector2 pos) 
        {
            m_txr = txr;
            m_position = pos;
            m_alpha = 0;
            m_fadeSpeed = 5;
        }

        // Fade in: Gradually reduce the alpha value of the mask to 0
        public void fadein()
        {
            if (m_alpha >= 0 + m_fadeSpeed)
            {
                m_alpha -= m_fadeSpeed;
            }
        }

        // Fade out: Gradually increase the alpha value of the mask to 255 
        public void fadeout()
        {
            if (m_alpha <= 255 - m_fadeSpeed)
            {
                m_alpha += m_fadeSpeed;
            }
        }

        // Update the mask with the camera position
        public void updateme(Vector2 cam_Pos)
        {
            m_position = cam_Pos;
        }

        // Draw the mask
        public void drawmealpha(SpriteBatch sb)
        {
            sb.Draw(m_txr, m_position, new Color(255, 255, 255, m_alpha));
        }
    }
}
