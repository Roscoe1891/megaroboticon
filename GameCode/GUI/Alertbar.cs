﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Megaroboticon
{
    /// <summary>
    /// The Alertbar is a child of the GUI class.
    /// It appears at the left hand side of the screen.
    /// It is a graphical representation of the current alert state.
    /// It is composed of a fixed backing texture and a 'fill' texture, which varies in height. 
    /// </summary>
    class Alertbar : GUI
    {
        // CLASS VARIABLES

        // Static variables
        // Positioning of the alertbar 'fill'
        public static int maxHeight = 550;
        public static int default_Y = 600;
      
        // Adjustment to fit inside the backing texture
        public static int adjustToFit_X = 4;
        public static int adjustToFit_Y = 3;
        
        // height and position of the 'fill'
        private int m_currHeight;
        private Vector2 m_myNewPos;
        
        // Texture and position of the backing texture
        private Vector2 m_backingPos;
        private Texture2D m_backing_txr;
       
        // CONSTRUCTOR
        public Alertbar(Texture2D txr, Texture2D backing_txr, Vector2 pos, int screenWidth, int screenHeight)
            : base(txr, pos, screenWidth, screenHeight)
        {
            m_currHeight = 0;
            m_myNewPos = pos;
            m_backing_txr = backing_txr;
        }

        // UPDATE    
        public void updateme(int alertLevel, int alertTrigger, Vector2 cam_Pos)
        {
            // Alertbar 'fill' height increases in line with the current alert level.
            // (Up to it's defined maximum height).
            if (alertLevel <= alertTrigger)
            {
                // Calculate the new height
                m_currHeight = alertLevel * (maxHeight / (alertTrigger));
            }
            else
            {
                m_currHeight = maxHeight;
            }
            // Set the new height for the 'fill'
            setHeight(m_currHeight);

            // Calculate and set new position of the 'fill'
            m_myNewPos = new Vector2(cam_Pos.X + (m_backing_txr.Width/2) + adjustToFit_X, cam_Pos.Y + (default_Y - Height + adjustToFit_Y));
            setPosition(m_myNewPos);

            // Set new position of the backing texture
            m_backingPos = new Vector2(cam_Pos.X + Width/2, cam_Pos.Y + (default_Y - maxHeight));

        }

        // Draw the backing texture
        public void drawbacking(SpriteBatch sb)
        {
            sb.Draw(m_backing_txr, new Rectangle((int)m_backingPos.X, (int)m_backingPos.Y, m_backing_txr.Width, m_backing_txr.Height), Color.White);
        }

       
    }
}
