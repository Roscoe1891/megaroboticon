﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Megaroboticon
{
    /// <summary>
    /// A child of the GUI class.
    /// This appears at the start of the game and is the main display mode for navigating through the start menu.
    /// It composed of a background splash screen with the static 'Megaroboticon' tower and the scrolling city skyline.
    /// It also contains its own fade mask as the fadein at the start of the game is handled differently to the fades executed by the FadeMask class.
    /// </summary>
    class Startscreen : GUI
    {
        // Texture & Position
        private Texture2D m_scrollingScreen;
        private Vector2 m_scrollPos;

        // Tower Texture & Position
        private Texture2D m_tower;
        private Vector2 m_towerPos;

        // Mask Texture and Alpha Value
        private Texture2D m_mask;
        private int m_maskAlpha;
       
        // Mask Fade Trigger Max and Trigger Counter
        private int m_alphaFade;
        private int m_alphaTrigger;
       
        // CONSTRUCTOR
        public Startscreen(Texture2D txr, Texture2D scrollScreen, Texture2D tower, Texture2D mask, Vector2 pos, int screenWidth, int screenHeight)
            : base(txr, pos, screenWidth, screenHeight)
        {
            // Texture & Position
            m_scrollingScreen = scrollScreen;
            m_scrollPos = new Vector2(0, screenHeight - m_scrollingScreen.Height);
            // Tower Texture & Position
            m_tower = tower;
            m_towerPos = new Vector2(700, screenHeight - m_tower.Height);
            // Mask properties
            m_mask = mask;
            m_maskAlpha = 255;
            m_alphaTrigger = 0;
            m_alphaFade = 2;
        }

        //UPDATE
        public void updateme()
        {
            // Move the scrolling part of the start screen to the left
            m_scrollPos.X--;
            // If the scrolling screen has scrolled to the end, reset it's position
            if (m_scrollPos.X < -m_scrollingScreen.Width/2)
            {
                m_scrollPos.X += m_scrollingScreen.Width/2;
            }

            // If the Mask's alpha value is greater than 0
            if (m_maskAlpha > 0)
            {
                // Increase the alpha trigger counter
                m_alphaTrigger ++;
            }

            // If the the alpha trigger counter reaches the maximum
            if (m_alphaTrigger == m_alphaFade)
            {
                // If the the Mask's alpha value is greater than 0
                if (m_maskAlpha > 0)
                {
                    m_maskAlpha--;
                    m_alphaTrigger = 0;
                }
            }
        }

        // Complete the fade-in for the introduction (allows for interrupt to the fade-in)
        public void introfadecomplete()
        {
            m_maskAlpha = 0;
        }

        // Draw the skyline
        public void drawskyline(SpriteBatch sb)
        {
            if (m_maskAlpha > 0)
            {
                sb.Draw(m_mask, Vector2.Zero, new Color(255, 255, 255, m_maskAlpha));
            }
            sb.Draw(m_tower, m_towerPos, Color.White);
            sb.Draw(m_scrollingScreen, m_scrollPos, Color.White);
        }
    }
}
