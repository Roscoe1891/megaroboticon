﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;


namespace Megaroboticon
{
    class GUI
    {
        /// <summary>
        /// Parent to the Alertbar, Console, ConvWindow, ObjWindow & Startscreen classes
        /// It contains the core information for these GUI elements such as position, size and texture.
        /// </summary>
        
        // CLASS VARIABLES
        //Texture, position & size
        private Texture2D m_txr;
        private Vector2 m_position;
        private int m_Width;
        public int Width
        {
            get { return m_Width; }
        }
        private int m_Height;
        public int Height
        {
            get { return m_Height; }
        }

        // Screen width & height
        private int m_screenWidth;
        public int ScreenWidth
        {
            get { return m_screenWidth; }
        }
        private int m_screenHeight;
        public int ScreenHeight
        {
            get { return m_screenHeight; }
        }
      
        // CONSTRUCTOR
        public GUI(Texture2D txr, Vector2 pos, int screenWidth, int screenHeight)
        {
            m_txr = txr;
            m_position = pos;
            m_screenWidth = screenWidth;
            m_screenHeight = screenHeight;
            m_Width = txr.Width;
            m_Height = txr.Height;
        }

        // Used by alert bar which has variables height
        public void setHeight(int newHeight)
        {
            m_Height = newHeight;
        }

        // Set a new position
        public void setPosition(Vector2 newPosition)
        {
            m_position = newPosition;
        }

        // Draw
        public void drawme(SpriteBatch sb)
        {
            sb.Draw(m_txr, new Rectangle((int)m_position.X, (int)m_position.Y, m_Width, m_Height), Color.White);
        }
    }
}
