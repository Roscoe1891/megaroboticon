using System;
using System.IO;
using System.IO.IsolatedStorage;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Storage;


namespace Megaroboticon
{
    /// <summary>
    /// This is the main game class.
    /// It acts as a state and object manager and handles the core game events and interactions between game objects.
    /// </summary>

    // STRUCTURES
    // High Score Data
    [Serializable]
    public struct HighScoreData
    {
        public string[] PlayerName;
        public int[] Score;

        public int Count;
   
        public HighScoreData(int count)
        {
            PlayerName = new String[count];
            Score = new int[count];

            Count = count;
        }
    }

    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        // ENUMERATOR TYPES
        // Game State
        public enum GameState { StartState, PlayState, PauseState, WinState, LossState }
        // Start Screens
        public enum StartScreen {Title, Menu, EnterName, HighScores, HowToPlay, Credits, Intro }
        // Play States
        public enum PlayState { Move, Hack, Talk, Elevator }
        // Levels
        public enum PlayLevel { LV0, LV1, LV2, LV3, LV4 }
        // Security Status
        public enum SecurityStatus { Normal, Comprimised }

        // GAME VARIABLES
#if DEBUG
        public static SpriteFont debugFont;
#endif
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        // Random Number Generator
        public static readonly Random RNG = new Random();

        // Player name entry
        private char[] m_myName;
        private int m_selectedChar;
        private String m_playerName;

        // Camera
        private Camera2D m_cam;

        // Game State
        private GameState m_currentState;
        private StartScreen m_currentStartScreen;
        private PlayState m_currentPlayState;

        // High Score
        public readonly string HighScoresFilename = "highscores.lst";
        private HighScoreData currentHighScore;

        // Start Menu
        private String[] startMenu;
        private int m_selectedStartMenuItem;

        // GUI elements
        // Start Screen
        private Startscreen m_startScreen;
        private int m_startScreenTimer;
        private int m_startscreenTrigger;
        // Alert bar
        private Alertbar m_alertBar;
        // Console
        private Console m_console;
        // Conversation Window
        private ConvWindow m_convWindow;
        private Texture2D m_convWinTxr;
        private SpriteFont m_convFont;
        private SpriteFont m_convFontSmall;
        // Objective Window
        private ObjWindow m_objWindow;
        private Texture2D m_objMarkerTxr;
        private String m_failMessage;
        // Fade in/out Mask
        private FadeMask m_fadeMask;
        private bool m_fading;
        // Elevator
        private SpriteFont m_elevatorFont;
        private int m_elevatorTime;
        private int m_elevatorTimer;

        // ACTORS
        private PC m_player1;

        private Texture2D m_sentry_bluetxr;
        private Texture2D m_sentry_redtxr;
        private Texture2D m_sentry_blacktxr;

        private Texture2D m_missile_txr;

        // Score
        private int m_timeThisLevel;
        private double m_playerScore;
   
        // ALERT LEVEL
        // Security State reference
        private SecurityStatus m_currSecState;
        public SecurityStatus SecState
        {
            get { return m_currSecState; }
        }
        private SecurityStatus m_oldSecState;
        // Current Alert Level
        private int m_alertLevel;
        // Alert Level Trigger (changes security status)
        private int m_alertTrigger;
        // Is player currently being seen?
        private bool m_playerSeen;
        
        // LEVEL PROGRESS
        // Play Level Reference
        private PlayLevel m_currentPlayLevel;
        // Level reference
        private Level m_currentLevel;
        // Map reference
        private int m_currentFloor;
        // List of location of hacked terminals
        private List<Point> m_terminalsHacked;
        // Have new sentries been spanwed this level?
        private bool m_newSentriesSpawned;
 
        // Game Pad state Reference
        private GamePadState m_padCurr;
        private GamePadState m_padOld;
        
        // ENVIRONMENT
        // Side size of one square tile (pixels)
        public static readonly int TileSize = 60;
        // List of map tiles
        private List<Texture2D> m_tiles;
        // Map reference
        private Map m_currentMap;

        // SOUND
        // Sound Effects
        SoundEffect laser;
        SoundEffect bingBong;
        SoundEffect alarm;
        SoundEffect shutDown;
        SoundEffect doors;
        SoundEffect terminalHacked;
        // Soundtrack
        Song titleMusic;
        Song levelMusic;
        Song powerDownMusic;
        Song victoryMusic;
        // Is there music playing now?
        private bool musicPlaying;

        public Game1()
        {
            // Assign graphics and content
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            // Add Title, define screen width & height
            Window.Title = "Megaroboticon";
            graphics.PreferredBackBufferWidth = 1280;
            graphics.PreferredBackBufferHeight = 720;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            #region Tile List
            m_tiles = new List<Texture2D>();
            m_tiles.Add(Content.Load<Texture2D>("Tiles\\Void"));
            m_tiles.Add(Content.Load<Texture2D>("Tiles\\Floor"));
            m_tiles.Add(Content.Load<Texture2D>("Tiles\\Wall"));
            m_tiles.Add(Content.Load<Texture2D>("Tiles\\TableT"));
            // Terminal
            m_tiles.Add(Content.Load<Texture2D>("Tiles\\Floor"));
            // Elevator
            m_tiles.Add(Content.Load<Texture2D>("Tiles\\Floor"));
            // Alarmed Floor
            m_tiles.Add(Content.Load<Texture2D>("Tiles\\AlarmedFloor"));
            // Closed Doors: Vertical & Horizontal
            m_tiles.Add(Content.Load<Texture2D>("Tiles\\SecDoorsV"));
            m_tiles.Add(Content.Load<Texture2D>("Tiles\\SecDoorsH"));
            // Blocked for sentries
            m_tiles.Add(Content.Load<Texture2D>("Tiles\\Floor"));
            // Boundary for sentries
            m_tiles.Add(Content.Load<Texture2D>("Tiles\\Floor"));
            // Table Tiles
            m_tiles.Add(Content.Load<Texture2D>("Tiles\\TableB"));
            m_tiles.Add(Content.Load<Texture2D>("Tiles\\TableBL"));
            m_tiles.Add(Content.Load<Texture2D>("Tiles\\TableBR"));
            m_tiles.Add(Content.Load<Texture2D>("Tiles\\TableL"));
            m_tiles.Add(Content.Load<Texture2D>("Tiles\\TableR"));
            m_tiles.Add(Content.Load<Texture2D>("Tiles\\TableTL"));
            m_tiles.Add(Content.Load<Texture2D>("Tiles\\TableTR"));
            // Terminal Tiles
            m_tiles.Add(Content.Load<Texture2D>("Tiles\\TerminalB"));
            m_tiles.Add(Content.Load<Texture2D>("Tiles\\TerminalL"));
            m_tiles.Add(Content.Load<Texture2D>("Tiles\\TerminalR"));
            m_tiles.Add(Content.Load<Texture2D>("Tiles\\TerminalT"));
            // LV0 receptionist
            m_tiles.Add(Content.Load<Texture2D>("Tiles\\Receptionist"));
            m_tiles.Add(Content.Load<Texture2D>("Tiles\\ReceptionistTerminal"));
            // Elevator Tiles
            m_tiles.Add(Content.Load<Texture2D>("Tiles\\ElevatorB"));
            m_tiles.Add(Content.Load<Texture2D>("Tiles\\ElevatorT"));
            m_tiles.Add(Content.Load<Texture2D>("Tiles\\ElevatorR"));
            m_tiles.Add(Content.Load<Texture2D>("Tiles\\ElevatorL"));
            #endregion

            // Game State
            m_currentState = GameState.StartState;
            // Start Screen
            m_currentStartScreen = StartScreen.Title;
            m_startScreenTimer = 0;
            m_startscreenTrigger = 640;
            // Play State
            m_currentPlayState = PlayState.Move;
            // Camera
            m_cam = new Camera2D(Vector2.Zero, graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight);
            // Map
            m_currentMap = new Map(m_currentPlayLevel, Content.Load<Texture2D>("GUI\\MarkerAnim"), Content.Load<SoundEffect>("Sounds\\bingbong"), Content.Load<SoundEffect>("Sounds\\door"));
            // Player 
            m_player1 = new PC(new Point(9, 22), Content.Load<Texture2D>("Characters\\PlayerAnim"), Direction.North, 12, 18, 5f);
            m_myName = new char[6] { 'A', 'A', 'A', 'A', 'A', 'A' };
            m_selectedChar = 0;

            #region MUSIC
            musicPlaying = false;
            MediaPlayer.Volume = 1f;
            MediaPlayer.IsRepeating = true;
            SoundEffect.MasterVolume = 0.2f;
            #endregion

            #region Start Menu
            // Initialise and populate the start menu & selector

            startMenu = new String[5];
            startMenu[0] = "START GAME";
            startMenu[1] = "HIGH SCORES";
            startMenu[2] = "HOW TO PLAY";
            startMenu[3] = "CREDITS";
            startMenu[4] = "QUIT";
            m_selectedStartMenuItem = 0;

            #endregion

            #region Level Settings;
            // Zero level setting

            m_timeThisLevel = 0;
            m_playerScore = 0;

            m_currentPlayLevel = PlayLevel.LV0;
            m_currentLevel = new Level(m_currentPlayLevel, m_player1, m_sentry_bluetxr, m_sentry_blacktxr, m_missile_txr, laser, shutDown);
            m_terminalsHacked = new List<Point>();
            m_currentFloor = 0;
            m_newSentriesSpawned = false;
  
            #endregion

            #region Security Settings;
            // Set default security settings

            m_alertLevel = 0;
            m_alertTrigger = 180;
            m_currSecState = SecurityStatus.Normal;
            m_oldSecState = SecurityStatus.Normal;
            #endregion

            #region GUI elements
            // Start Screen
            m_startScreen = new Startscreen(Content.Load<Texture2D>("GUI\\StartSplash"), Content.Load<Texture2D>("GUI\\Skyline"), Content.Load<Texture2D>("GUI\\Tower"), Content.Load<Texture2D>("GUI\\Mask"), Vector2.Zero, graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight);
            // console
            m_console = new Console(Content.Load<Texture2D>("GUI\\Console"), Vector2.Zero, graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight, Content.Load<SpriteFont>("Fonts\\ConsoleFont"), Content.Load<SpriteFont>("Fonts\\Verdana10"), Content.Load<Texture2D>("GUI\\Timebar"), m_currentMap);
            // Alertbar
            m_alertBar = new Alertbar(Content.Load<Texture2D>("GUI\\Alertbar"), Content.Load<Texture2D>("GUI\\Alertbarbacking"), Vector2.Zero, graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight);
            // Objectives Window
            m_objWindow = new ObjWindow(Content.Load<Texture2D>("GUI\\ObjWindow"), Vector2.Zero, graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight, Content.Load<SpriteFont>("Fonts\\ObjFont"));
            // Fade Mask
            m_fadeMask = new FadeMask(Content.Load<Texture2D>("GUI\\Mask"), Vector2.Zero);
            m_fading = false;
            // Elevator
            m_elevatorTime = 120;
            m_elevatorTimer = 0;
            // Failure message
            m_failMessage = "";
            #endregion

            #region HIGH SCORE LIST
            // Get the path of the save game
            string fullpath = "highscores.lst";

            //If the file doesn't exist...
            if (!File.Exists(fullpath))
            {
                // Make a default one
                HighScoreData data = new HighScoreData(5);
                for (int i = 0; i < data.Count; i++)
                {
                    data.PlayerName[i] = "00000" + (i+1);
                    data.Score[i] = 0;
                }
                // Save the new high score list
                SaveHighScores(data, HighScoresFilename);
            }
            #endregion

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
#if DEBUG
            debugFont = Content.Load<SpriteFont>("Fonts\\Arial07");
#endif
  
            // Load Sentry Textures
            m_sentry_bluetxr = Content.Load<Texture2D>("Characters\\RoboSentryAnimBlue");
            m_sentry_redtxr = Content.Load<Texture2D>("Characters\\RoboSentryAnimRed");
            m_sentry_blacktxr = Content.Load<Texture2D>("Characters\\RoboSentryAnimBlack");

            // Load Missile Texture
            m_missile_txr = Content.Load<Texture2D>("Characters\\Eblast");

            // Load UI Textures & fonts
            m_convWinTxr = Content.Load<Texture2D>("GUI\\ConvWindow");
            m_convFont = Content.Load<SpriteFont>("Fonts\\ConvFont");
            m_convFontSmall = Content.Load<SpriteFont>("Fonts\\ConvFontSml");
            m_objMarkerTxr = Content.Load<Texture2D>("GUI\\MarkerAnim");
            m_elevatorFont = Content.Load<SpriteFont>("Fonts\\ElevatorFont");

            // Load Sound Effects
            laser = Content.Load<SoundEffect>("Sounds\\laser");
            bingBong = Content.Load<SoundEffect>("Sounds\\bingbong");
            alarm = Content.Load<SoundEffect>("Sounds\\alarm");
            shutDown = Content.Load<SoundEffect>("Sounds\\shutdown");
            doors = Content.Load<SoundEffect>("Sounds\\door");
            terminalHacked = Content.Load<SoundEffect>("Sounds\\computer");

            // Load Songs
            titleMusic = Content.Load<Song>("Songs\\title");
            levelMusic = Content.Load<Song>("Songs\\levels");
            powerDownMusic = Content.Load<Song>("Songs\\atmosphere");
            victoryMusic = Content.Load<Song>("Songs\\victory");
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Update Current Gamepad State
            m_padCurr = GamePad.GetState(PlayerIndex.One);
            
            // Update fade mask 
            m_fadeMask.updateme(m_cam.Position);

            #region START STATE
            if (m_currentState == GameState.StartState)
            {
                // If there is no music playing, play the title music
                if (musicPlaying == false)
                {
                    MediaPlayer.Play(titleMusic);
                    musicPlaying = true;
                }

                // update the start screen
                m_startScreen.updateme();
              
                // Start State Navigation
                // (Behaviour depedant on the current start screen)
                switch (m_currentStartScreen)
                {
                    // TITLE Start Screen
                    case StartScreen.Title:
                        {
                           // The start screen will gradually fade in on starting the game
                           if (m_startScreenTimer < m_startscreenTrigger)
                           {
                               m_startScreenTimer++;
                           }
                           // Once it has faded in, switch to the Menu
                           // (The trigger is timed to coincide with when the drums come in on the title music)
                           if (m_startScreenTimer >= m_startscreenTrigger)
                           {
                               m_startScreen.introfadecomplete();
                               m_currentStartScreen = StartScreen.Menu;
                           }
                            
                            // Switch to menu on pressing Start (bypassing fade in above)
                            if ((m_padCurr.Buttons.Start == ButtonState.Pressed) && (m_padOld.Buttons.Start != ButtonState.Pressed))
                            {
                                m_startScreen.introfadecomplete();
                                m_currentStartScreen = StartScreen.Menu;
                                m_padOld = m_padCurr;
                            }
                            break;
                        }
                    // MENU Start Screen
                    case StartScreen.Menu:
                        {
                            // Allow selection from the menu using Up/Down on the Dpad
                            if ((m_padCurr.DPad.Down == ButtonState.Pressed) && (m_padOld.DPad.Down != ButtonState.Pressed))
                            {
                                if (m_selectedStartMenuItem < startMenu.Length - 1)
                                {
                                    m_selectedStartMenuItem++;
                                }
                            }
                            if ((m_padCurr.DPad.Up == ButtonState.Pressed) && (m_padOld.DPad.Up != ButtonState.Pressed))
                            {
                                if (m_selectedStartMenuItem > 0)
                                {
                                    m_selectedStartMenuItem--;
                                }
                            }

                            // If Start is pressed
                            if ((m_padCurr.Buttons.Start == ButtonState.Pressed) && (m_padOld.Buttons.Start != ButtonState.Pressed))
                            {
                                // Switch to the currently selected screen
                                switch (m_selectedStartMenuItem)
                                {
                                    // 0: Start Game
                                    case 0:
                                        {
                                            // Prompt for Name Entry before Starting
                                            m_currentStartScreen = StartScreen.EnterName;
                                            m_padOld = m_padCurr;
                                            break;
                                        }
                                    // 1: High Scores
                                    case 1:
                                        {
                                            // Load the high score list into the 'current high score' variable
                                            currentHighScore = LoadHighScores(HighScoresFilename);

                                            m_currentStartScreen = StartScreen.HighScores;
                                            break;
                                        }
                                    // 2: How to Play
                                    case 2:
                                        {
                                            m_currentStartScreen = StartScreen.HowToPlay;
                                            break;
                                        }
                                    // 3: Credits
                                    case 3:
                                        {
                                            m_currentStartScreen = StartScreen.Credits;
                                            break;
                                        }
                                    // 4: Quit
                                    case 4:
                                        {
                                            this.Exit();
                                            break;
                                        }
                                }

                            }
                            break;
                        }
                    // ENTER NAME Start Screen 
                    case StartScreen.EnterName:
                        {
                            // Allow Name Entry using the Dpad
                            // Left or right to select character place
                            if ((m_padCurr.DPad.Right == ButtonState.Pressed) && (m_padOld.DPad.Right != ButtonState.Pressed))
                            {
                                if (m_selectedChar < m_myName.Length - 1)
                                    m_selectedChar++;
                            }
                            if ((m_padCurr.DPad.Left == ButtonState.Pressed) && (m_padOld.DPad.Left != ButtonState.Pressed))
                            {
                                if (m_selectedChar > 0)
                                    m_selectedChar--;
                            }

                            // Up or down to select individual character
                            if ((m_padCurr.DPad.Up == ButtonState.Pressed) && (m_padOld.DPad.Up != ButtonState.Pressed))
                            {
                                if (m_myName[m_selectedChar] < 126)
                                    m_myName[m_selectedChar]++;
                            }
                            if ((m_padCurr.DPad.Down == ButtonState.Pressed) && (m_padOld.DPad.Down != ButtonState.Pressed))
                            {
                                if (m_myName[m_selectedChar] > 32)
                                    m_myName[m_selectedChar]--;   
                            }

                            // Press Start to confirm
                            if ((m_padCurr.Buttons.Start == ButtonState.Pressed) && (m_padOld.Buttons.Start != ButtonState.Pressed))
                            {
                                // Set the name entered as a string
                                m_playerName = "" + m_myName[0] + m_myName[1] + m_myName[2] + m_myName[3] + m_myName[4] + m_myName[5];
                                // Reset the console and set the intro text using the player name
                                m_console.resetConsole();
                                m_console.setintrotext(m_playerName);

                                // Start fade
                                m_fading = true;
                            }

                            // If currently doing a fade in or out (other than the initial fade in)
                            if (m_fading == true)
                            {
                                // Use the fade mask to fade out
                                m_fadeMask.fadeout();
                                // When fully faded out
                                if (m_fadeMask.Alpha == 255)
                                {
                                    // Proceed to the intro screen
                                    m_console.setConsole(m_cam.Position, m_currentPlayLevel, m_currentMap, m_terminalsHacked.Count);
                                    m_currentStartScreen = StartScreen.Intro;
                                }
                            }

                            // Allow return to menu
                            if ((m_padCurr.Buttons.Back == ButtonState.Pressed) && (m_padOld.Buttons.Back != ButtonState.Pressed))
                            {
                                m_currentStartScreen = StartScreen.Menu;
                            }
                            break;
                        }
                    // INTRO Start State 
                    case StartScreen.Intro:
                        {
                            // If currently doing a fade in or out (other than the initial fade in)
                            if (m_fading == true)
                            {
                                // Use the fade mask to fade in
                                m_fadeMask.fadein();
                                // When fully faded in
                                if (m_fadeMask.Alpha == 0)
                                {
                                    // No longer doing a fade in or out 
                                    m_fading = false;
                                }
                            }

                            // On pressing start
                            if ((m_padCurr.Buttons.Start == ButtonState.Pressed) && (m_padOld.Buttons.Start != ButtonState.Pressed))
                            {
                                // Stop fade
                                m_fading = false;

                                // Stop the music
                                MediaPlayer.Stop();
                                musicPlaying = false;

                                // If not at Level 0
                                if (m_currentPlayLevel != PlayLevel.LV0)
                                {
                                    // Set level to 0
                                    m_currentPlayLevel = PlayLevel.LV0;
                                    newLevel();
                                }

                                // Switch to play state
                                m_currentState = GameState.PlayState;

                                // Switch Start Screen to menu (ready for when player returns to this screen)
                                m_currentStartScreen = StartScreen.Menu;

                                m_padOld = m_padCurr;
                            }
                            break;
                        }
                    // HIGH SCORE Start Screen
                    case StartScreen.HighScores:
                        {
                           

                            // Allow return to menu using 'back'
                            if ((m_padCurr.Buttons.Back == ButtonState.Pressed) && (m_padOld.Buttons.Back != ButtonState.Pressed))
                            {
                                m_currentStartScreen = StartScreen.Menu;
                            }
                            break;
                        }
                    // HOW TO PLAY Start Screen
                    case StartScreen.HowToPlay:
                        {
                            // Allow return to menu using 'back'
                            if ((m_padCurr.Buttons.Back == ButtonState.Pressed) && (m_padOld.Buttons.Back != ButtonState.Pressed))
                            {
                                m_currentStartScreen = StartScreen.Menu;
                            }
                            break;
                        }
                    // CREDITS Start Screen 
                    case StartScreen.Credits:
                        {
                            // Allow return to menu using 'back'
                            if ((m_padCurr.Buttons.Back == ButtonState.Pressed) && (m_padOld.Buttons.Back != ButtonState.Pressed))
                            {
                                m_currentStartScreen = StartScreen.Menu;
                            }
                            break;
                        }
                }
            }
            #endregion

            #region PLAY STATE
            if (m_currentState == GameState.PlayState)
            {
                if (m_currentPlayState == PlayState.Move)
                // CENTRE CAMERA
                m_cam.updateme(m_player1.Position, m_padCurr);

                // If there is no music playing, play the level
                if (musicPlaying == false)
                {
                    MediaPlayer.Play(levelMusic);
                    musicPlaying = true;
                }

                #region PLAY STATE DEPENDANT UPDATES
                switch (m_currentPlayState)
                {
                    // In the Move Playstate
                    case PlayState.Move:
                        {

                            #region SWITCH TO PAUSE STATE
                            // Press start to switch
                            if ((m_padCurr.Buttons.Start == ButtonState.Pressed) && (m_padOld.Buttons.Start != ButtonState.Pressed))
                            {
                                // Change state and pause music
                                m_currentState = GameState.PauseState;
                                m_padOld = m_padCurr;
                                MediaPlayer.Pause();
                            }
                            #endregion

                            #region UPDATE GAME OBJECTS
                            // Only if not fading in or out
                            if (m_fading == false)
                            {
                                // UPDATE SENTRIES
                                foreach (Enemy sentry in m_currentLevel.Sentries)
                                {
                                    // (The sentries' eBlasts are also updated by this method)
                                    sentry.updateme(gameTime, m_currentMap, m_player1, SecState);
                                }

                                // UPDATE PLAYER 1
                                m_player1.updateme(gameTime, m_currentMap, m_padCurr, m_padOld);

                                //UPDATE Objective Window GUI
                                m_objWindow.updateme(m_cam.Position);
                            }

                            #endregion

                            #region UPDATE ALERT LEVEL & SECURITY STATE

                            foreach (Enemy sentry in m_currentLevel.Sentries)
                            {
                                // If any sentry can see the player
                                if ((sentry.LOS(m_player1.TilePosition, m_currentMap, m_currSecState)) && (m_currentLevel.SecurityShutdown == false))
                                {
                                    // The alert level increases (to a max of twice the trigger level) 
                                    if (m_alertLevel <= m_alertTrigger * 2)
                                    {
                                        m_alertLevel++;
                                    }
                                }

                                // Player is classed as unseen unless at least one sentry can see him
                                // And as long as the level's security shutdown has not been run
                                m_playerSeen = false;
                                if ((sentry.LOS(m_player1.TilePosition, m_currentMap, m_currSecState)) && (m_currentLevel.SecurityShutdown != true))
                                {
                                    m_playerSeen = true;
                                    break;
                                }
                            }

                            // Update the alert bar GUI
                            m_alertBar.updateme(m_alertLevel, m_alertTrigger, m_cam.Position);

                            // If no enemies can see the player, reduce the alert level
                            if ((m_alertLevel > 0) && (m_playerSeen == false))
                            {
                                m_alertLevel--;
                            }

                            // Alert level spikes if player is standing on an alarmed tile
                            if (m_currentMap.isAlarmed(m_player1.TilePosition) && (m_alertLevel <= m_alertTrigger * 2))
                            {
                                m_alertLevel += 10;
                            }

                            // if Alert Level reaches or exceeds maximum - set Security Status to Comprimised
                            if (m_alertLevel >= m_alertTrigger)
                            {
                                m_currSecState = SecurityStatus.Comprimised;
                            }

                            // if Alert Level is 0, set Security Status to Normal
                            if (m_alertLevel == 0)
                            {
                                m_currSecState = SecurityStatus.Normal;
                            }

                            // if security status has just turned comprimised, change sentries to red & sound alarm
                            if ((m_currSecState == SecurityStatus.Comprimised) && (m_oldSecState != SecurityStatus.Comprimised))
                            {
                                alarm.Play();
                                foreach (Enemy sentry in m_currentLevel.Sentries)
                                {
                                    sentry.ChangeTextureRed(m_sentry_redtxr);
                                }
                            }

                            // if security status has just turned normal, change sentries to blue
                            // As long as the level's security shutdown has not been run
                            if (m_currentLevel.SecurityShutdown == false)
                            {
                                if ((m_currSecState == SecurityStatus.Normal) && (m_oldSecState != SecurityStatus.Normal))
                                {
                                    foreach (Enemy sentry in m_currentLevel.Sentries)
                                    {
                                        sentry.ChangeTextureBlue(m_sentry_bluetxr);
                                    }
                                }
                            }

                            #endregion

                            #region CHECK FOR COLLISIONS
                            // As long the "Security Shutdown" procedure has not been run
                            if (m_currentLevel.SecurityShutdown == false)
                            {
                                // Check all sentries
                                foreach (Enemy sentry in m_currentLevel.Sentries)
                                {
                                    // For collision between sentries and player
                                    if (sentry.CollisionRect.Intersects(m_player1.CollisionRect))
                                    {
                                        // Set the failure message and switch to loss state
                                        m_failMessage = "You got Caught!";
                                        m_currentState = GameState.LossState;
                                    }
                                    // For collision between eblast and player
                                    foreach (Missile eBlast in sentry.eBlasts)
                                    {
                                        if (eBlast.CollisionRect.Intersects(m_player1.CollisionRect))
                                        {
                                            // Set the failure message and switch to loss state
                                            m_failMessage = "You got Shot!";
                                            m_currentState = GameState.LossState;
                                        }
                                    }
                                }
                            }
                            #endregion

                            #region LV 2 EXTRA UPDATES
                            // On hacking 1st terminal...
                            if (m_terminalsHacked.Count == 1)
                            {
                                //Spawn new sentries 
                                if (m_newSentriesSpawned == false)
                                {
                                    m_currentLevel.spawnnewsentries(m_currentPlayLevel);
                                    m_newSentriesSpawned = true;
                                }
                            }
                            #endregion

                            #region LV 3 EXTRA UPDATES
                            if (m_currentPlayLevel == PlayLevel.LV3)
                            {
                                // Update guards
                                foreach (Enemy guard in m_currentLevel.Guards)
                                {
                                    guard.updateme(gameTime, m_currentMap, m_player1, SecState);

                                    // if security status has just turned comprimised, change guards to red
                                    if ((m_currSecState == SecurityStatus.Comprimised) && (m_oldSecState != SecurityStatus.Comprimised))
                                        guard.ChangeTextureRed(m_sentry_redtxr);

                                    // if security status has just turned normal, change guards to blue
                                    // As long as the level's security shutdown has not been run
                                    if (m_currentLevel.SecurityShutdown == false)
                                    {
                                        if ((m_currSecState == SecurityStatus.Normal) && (m_oldSecState != SecurityStatus.Normal))
                                            guard.ChangeTextureBlue(m_sentry_bluetxr);
                                    }
                                }


                                // Check guards for collisions
                                if (m_currentLevel.SecurityShutdown == false)
                                {
                                    foreach (Enemy guard in m_currentLevel.Guards)
                                    {
                                        // Between a guard and player
                                        if (guard.Position == m_player1.Position)
                                        {
                                            m_currentState = GameState.LossState;
                                        }
                                        // Between eblast and player
                                        foreach (Missile eBlast in guard.eBlasts)
                                        {
                                            if (eBlast.TilePosition == m_player1.TilePosition)
                                            {
                                                m_currentState = GameState.LossState;
                                            }
                                        }
                                    }
                                }

                                // On hacking 1st terminal
                                if (m_terminalsHacked.Count == 1)
                                {
                                    // Spawn new sentries (if this has not been done already)
                                    if (m_newSentriesSpawned == false)
                                    {
                                        m_currentLevel.spawnnewsentries(m_currentPlayLevel);
                                        m_newSentriesSpawned = true;
                                    }
                                }

                            }
                            #endregion

                            #region LV 4 EXTRA UPDATES (VICTORY)
                            // Win the game on hacking the level 4 terminal
                            if (m_currentPlayLevel == PlayLevel.LV4)
                            {
                                if (m_terminalsHacked.Count == m_currentLevel.TerminalsThisFloor)
                                {
                                    m_fading = true;  
                                }

                                if (m_fading == true)
                                {
                                    m_fadeMask.fadeout();
                                    if (m_fadeMask.Alpha == 255)
                                    {
                                        SaveHighScore();
                                        m_console.setwintext(m_playerName, m_playerScore);
                                        MediaPlayer.Stop();
                                        musicPlaying = false;
                                        m_currentState = GameState.WinState;
                                        m_padOld = m_padCurr;
                                    }
                                }
                                   
                    
                            }
                            #endregion

                            #region UPDATE LEVEL PROGRESS

                            // Increase level timer
                            m_timeThisLevel++;

                            // Check for having reached a new terminal
                            if (m_currentMap.isNewTerminal(m_player1.TilePosition, m_terminalsHacked))
                            {
                                if (m_currentPlayLevel == PlayLevel.LV0)
                                {
                                    if (m_padCurr.Buttons.RightShoulder == ButtonState.Pressed && m_padOld.Buttons.RightShoulder != ButtonState.Pressed)
                                    {
                                        m_convWindow = new ConvWindow(m_convWinTxr, new Vector2(m_cam.Position.X + 150, m_cam.Position.Y + 585), graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight, m_playerName, m_convFont, m_convFontSmall);
                                        m_currentPlayState = PlayState.Talk;
                                    }
                                }

                                else if (m_padCurr.Buttons.RightShoulder == ButtonState.Pressed && m_padOld.Buttons.RightShoulder != ButtonState.Pressed)
                                {
                                    // Set Console and switch Play state to "Hack"
                                    m_console.setConsole(m_cam.Position, m_currentPlayLevel, m_currentMap, m_terminalsHacked.Count);
                                    terminalHacked.Play();
                                    m_currentPlayState = PlayState.Hack;
                                }
                            }


                            // Change level (on reaching an opened elevator)
                            if (((m_currentMap.isElevator(m_player1.TilePosition)) && (m_currentMap.ElevatorOpen == true)))
                            {
                                if (m_padCurr.Buttons.RightShoulder == ButtonState.Pressed && m_padOld.Buttons.RightShoulder != ButtonState.Pressed)
                                {
                                    // start a fade out
                                    m_fading = true;
                                }

                                if (m_fading == true)
                                {
                                    m_fadeMask.fadeout();
                                    if (m_fadeMask.Alpha == 255)
                                    {
                                        // Get the current floor number
                                        m_currentFloor = m_currentLevel.ThisFloor;
                                        // load the new level while in the elevator
                                        newLevel();
                                        m_currentPlayState = PlayState.Elevator;
                                    }
                                }
                            }
                            // Fade in at the start of the level
                            else if (m_fadeMask.Alpha != 0)
                            {
                                if (m_terminalsHacked.Count != m_currentLevel.TerminalsThisFloor)
                                m_fadeMask.fadein();
                            }
                            #endregion

                            break;
                        }
                    // In the Elevator Playstate
                    case PlayState.Elevator:
                        {
                            // Vibrate the game pad
                            GamePad.SetVibration(PlayerIndex.One, 1.0f, 0f);

                            // If doing a fade out or fade in
                            if (m_fading == true)
                            {
                                // If the elevator timer is 0
                                if (m_elevatorTimer == 0)
                                {
                                    // Do a fade in
                                    m_fadeMask.fadein();
                                    // Once faded in,
                                    if (m_fadeMask.Alpha == 0)
                                    {
                                        // No longer fading in, increase current floor 
                                        m_fading = false;
                                        m_currentFloor++;
                                    }
                                }
                                // Otherwise do a fadeout
                                else
                                {
                                    m_fadeMask.fadeout();
                                }
                            }

                            // If not doing a fade out or fade in
                            if (m_fading == false) 
                            {
                                // Increase the elevator timer
                                m_elevatorTimer++;
                                // If elevatior timer reaches maximum
                                if (m_elevatorTimer >= m_elevatorTime)
                                {
                                    // Start fade out / fade in
                                    m_fading = true;
                                    // Increase current floor
                                    m_currentFloor++;
                                }
                            }

                            // If elevator timer reaches half time
                            if (m_elevatorTimer == (m_elevatorTime / 2))
                            {
                                // Increase current floor
                                m_currentFloor++;
                            }
                            
                            // If elevator timer is greater than 0 and the screen has faded out
                            if ((m_elevatorTimer > 0) && (m_fadeMask.Alpha == 255))
                            {
                                // Reset game pad vibration, elevator and return to "Move" Playstate 
                                GamePad.SetVibration(PlayerIndex.One, 0f, 0f);
                                m_currentPlayState = PlayState.Move;
                                m_elevatorTimer = 0;
                                m_fading = false;
                            }

                            break;
                        }

                    // In the "Hack" Playstate
                    case PlayState.Hack:
                        {
                            // update the console
                            m_console.updateme(m_cam.Position, m_padCurr, m_padOld, m_currentLevel, m_currentMap);
                            // If the Hack failed
                            if (m_console.HackFail == true)
                            {
                                // Trigger the alarm, reset console & return to Playstate
                                m_alertLevel = m_alertTrigger * 2;
                                m_console.resetConsole();
                                m_currentPlayState = PlayState.Move;
                                
                                // On level 4 this also triggers the sentries to wake up in guard mode
                                if (m_currentPlayLevel == Game1.PlayLevel.LV4)
                                {
                                    m_currentLevel.setguard();
                                }
                            }
                            // If the hack was successful
                            if (m_console.HackSuccess == true)
                            {
                                // Add this terminal to the hacked terminals list, reset the console, complete objective and return to Playstate
                                m_terminalsHacked.Add(new Point(m_player1.TilePosition.X, m_player1.TilePosition.Y));

                                for (int i = 0; i < m_currentMap.ObjMarkers.Count; i++)
                                {
                                    if (m_player1.TilePosition == m_currentMap.ObjMarkers[i].Position)
                                    {
                                        m_currentMap.removeObjMarker(i);
                                    }
                                }
                                // Reset the console and update the objective
                                m_console.resetConsole();
                                m_objWindow.objectiveCompleted();
                                // Return to Move Playstate
                                m_currentPlayState = PlayState.Move;

                                // On level 3, activating the security shutdown changes the soundtrack
                                if ((m_currentPlayLevel == PlayLevel.LV3) && (m_currentLevel.SecurityShutdown == true))
                                {
                                    MediaPlayer.Stop();
                                    MediaPlayer.Play(powerDownMusic);
                                }
                            }
                            break;
                        }
                    // In the "Talk" Playstate
                    case PlayState.Talk:
                        {
                            // Update the conversation window
                            m_convWindow.updateme(m_padCurr, m_padOld, m_currentMap);

                            // If the conversation is over 
                            if (m_convWindow.ConvOver == true)
                            {
                                // Add new hacked terminal to the list (conversations count as terminals in game terms)
                                m_terminalsHacked.Add(new Point(m_player1.TilePosition.X, m_player1.TilePosition.Y));
                                // Find and remove the objective marjker for the conversation
                                for (int i = 0; i < m_currentMap.ObjMarkers.Count; i++)
                                {
                                    if (m_player1.TilePosition == m_currentMap.ObjMarkers[i].Position)
                                    {
                                        m_currentMap.removeObjMarker(i);
                                    }
                                }
                               
                                // Update the objective and return to play state
                                m_objWindow.objectiveCompleted();
                                m_currentPlayState = PlayState.Move;
                            }
                            break;
                        }
                }
                #endregion

                // Update old security state
                m_oldSecState = m_currSecState;
            }
            #endregion

            #region PAUSE STATE
            if (m_currentState == GameState.PauseState)
            {
                // Press start to resume play
                if ((m_padCurr.Buttons.Start == ButtonState.Pressed) && (m_padOld.Buttons.Start != ButtonState.Pressed))
                {
                    m_currentState = GameState.PlayState;
                    m_padOld = m_padCurr;
                    MediaPlayer.Resume();
                }

                // Quit to Start Menu using 'back'
                if ((m_padCurr.Buttons.Back == ButtonState.Pressed) && (m_padOld.Buttons.Back != ButtonState.Pressed))
                {
                    // Reset level settings, music and camera
                    m_currentPlayLevel = PlayLevel.LV0;
                    newLevel();
                    m_playerScore = 0;
                    MediaPlayer.Stop();
                    musicPlaying = false;
                    m_currentState = GameState.StartState;
                    m_cam.resetCamera();
                    m_padCurr = m_padOld;
                }
            }

            #endregion

            #region LOSS STATE
            if (m_currentState == GameState.LossState)
            {
                // Press Start to switch to start the level again
                if (m_padCurr.Buttons.Start == ButtonState.Pressed)
                {
                    newLevel();
                    m_currentState = GameState.PlayState;
                }
                // Quit to Start Menu using 'back'
                if ((m_padCurr.Buttons.Back == ButtonState.Pressed) && (m_padOld.Buttons.Back != ButtonState.Pressed))
                {
                    // Reset level settings, score, music & camera 
                    m_currentPlayLevel = PlayLevel.LV0;
                    newLevel();
                    m_playerScore = 0;
                    MediaPlayer.Stop();
                    musicPlaying = false;
                    m_currentState = GameState.StartState;
                    m_cam.resetCamera();
                    m_padCurr = m_padOld;
                }
            }
            #endregion

            #region WIN STATE
            if (m_currentState == GameState.WinState)
            {
                // If doing a fade in or fade out
                if (m_fading == true)
                {
                    // Use the fade mask to fade in
                    m_fadeMask.fadein();
                    // when fully faded in...
                    if (m_fadeMask.Alpha == 0)
                    {
                        // No longer doing a fade in or fade out
                        m_fading = false;
                    }
                }

                // If there is no music playing, play the victory music 
                if (musicPlaying == false)
                {
                    MediaPlayer.Play(victoryMusic);
                    musicPlaying = true;
                }

                // Escape to menu with 'start'
                if ((m_padCurr.Buttons.Start == ButtonState.Pressed) && (m_padOld.Buttons.Start != ButtonState.Pressed))
                {
                            // Reset score, music, level and camera
                            m_playerScore = 0;
                            MediaPlayer.Stop();
                            musicPlaying = false;
                            m_currentState = GameState.StartState;
                            m_cam.resetCamera();
                            m_currentPlayLevel = PlayLevel.LV0;
                            newLevel();
                }
            }
            #endregion

            // Update old Gamepad State
            m_padOld = m_padCurr;

            base.Update(gameTime);
        }

        // START A NEW LEVEL
        public void newLevel()
        {
            // If not starting new level due to losing or starting from scratch
            if ((m_currentState != GameState.LossState) && (m_currentState != GameState.StartState))
            {
                // Score the level
                m_playerScore += (1000000/m_timeThisLevel);

                // Then switch to the next level
                switch (m_currentPlayLevel)
                {
                    case PlayLevel.LV0:
                        {
                            m_currentPlayLevel = PlayLevel.LV1;
                            break;
                        }
                    case PlayLevel.LV1:
                        {
                            m_currentPlayLevel = PlayLevel.LV2;
                            break;
                        }
                    case PlayLevel.LV2:
                        {
                            m_currentPlayLevel = PlayLevel.LV3;
                            break;
                        }
                    case PlayLevel.LV3:
                        {
                            m_currentPlayLevel = PlayLevel.LV4;
                            break;
                        }
                }
            }

            // Generate the new Map & Level
            m_currentMap = new Map(m_currentPlayLevel, m_objMarkerTxr, bingBong, doors);
            m_currentLevel = new Level(m_currentPlayLevel, m_player1, m_sentry_bluetxr, m_sentry_blacktxr, m_missile_txr, laser, shutDown);

            // Reset level settings
            m_alertLevel = 0;
            m_currSecState = SecurityStatus.Normal;
            m_terminalsHacked.Clear();
            m_newSentriesSpawned = false;
            m_objWindow.resetObjective(m_currentPlayLevel);
        }

        // Save the High Score list
        public static void SaveHighScores(HighScoreData data, string filename)
        {
            // Get the path of the save game
            string fullpath = "highscores.lst";

            // Open the file, creating it if necessary
            FileStream stream = File.Open(fullpath, FileMode.OpenOrCreate);
            try
            {
                // Convert the object to XML data and put it in the stream
                XmlSerializer serializer = new XmlSerializer(typeof(HighScoreData));
                serializer.Serialize(stream, data);
            }
            finally
            {
                // Close the file
                stream.Close();
            }
        }

        // Load the High Score list
        public static HighScoreData LoadHighScores(string filename)
        {
            HighScoreData data;

            // Get the path of the save game
            string fullpath = "highscores.lst";

            // Open the file
            FileStream stream = File.Open(fullpath, FileMode.OpenOrCreate,
            FileAccess.Read);
            try
            {
                // Read the data from the file
                XmlSerializer serializer = new XmlSerializer(typeof(HighScoreData));
                data = (HighScoreData)serializer.Deserialize(stream);
            }
            finally
            {
                // Close the file
                stream.Close();
            }

            return (data);
        }

        private void SaveHighScore()
        {
            // Create the data to save
            HighScoreData data = LoadHighScores(HighScoresFilename);

            // Compare player score to the current high scores
            int scoreIndex = -1;
            for (int i = 0; i < data.Count; i++)
            {
                if (m_playerScore > data.Score[i])
                {
                    scoreIndex = i;
                    break;
                }
            }

            // If player has made the high scores list
            if (scoreIndex > -1)
            {
                // Do swaps to fit in the new entry
                for (int i = data.Count - 1; i > scoreIndex; i--)
                {
                    data.PlayerName[i] = data.PlayerName[i - 1];
                    data.Score[i] = data.Score[i - 1];
                }

                // Set new entry to player's name and score
                data.PlayerName[scoreIndex] = m_playerName;
                data.Score[scoreIndex] = (int)m_playerScore;
                
                // Save the new high socres list
                SaveHighScores(data, HighScoresFilename);
            }
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            // Set the background colour
            GraphicsDevice.Clear(Color.Black);

            // Begin Spritebatch
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, null, null, m_cam.getCam());

            #region ANY STATE OTHER THAN START OR WIN STATE
            if ((m_currentState != GameState.StartState) && (m_currentState != GameState.WinState))
            {
                // In any Play State other than the Elevator State
                if (m_currentPlayState != PlayState.Elevator)
                {
                    // Draw Map
                    m_currentMap.drawme(spriteBatch, m_tiles);

                    // Draw Objective Marker
                    foreach (ObjMarker obj in m_currentMap.ObjMarkers)
                    {
                        obj.drawme(spriteBatch, gameTime, m_currentState, m_currentPlayState);
                    }

                    // Draw Player
                    m_player1.drawme(spriteBatch, gameTime, m_currentState, m_currentPlayState);

                    // Draw Sentries
                    foreach (Enemy sentry in m_currentLevel.Sentries)
                    {
                        sentry.drawme(spriteBatch, gameTime, m_currentState, m_currentPlayState);
                        // Draw missiles
                        foreach (Missile eBlast in sentry.eBlasts)
                        {
                            eBlast.drawme(spriteBatch, gameTime);
                        }
                    }
                }

                #region PAUSE STATE
                if (m_currentState == GameState.PauseState)
                {
                    // Draw objective window help text (pause)
                    m_objWindow.drawpausehelp(spriteBatch);
                }
                #endregion

                #region PLAY STATE
                if (m_currentState == GameState.PlayState)
                {
                    // Depending on the play state...
                    switch (m_currentPlayState)
                    {
                        // In the Move Play State
                        case PlayState.Move:
                            {
                                // If playing level 3
                                if (m_currentPlayLevel == PlayLevel.LV3)
                                {
                                    // Draw guards
                                    foreach (Enemy guard in m_currentLevel.Guards)
                                    {
                                        guard.drawme(spriteBatch, gameTime, m_currentState, m_currentPlayState);
                                        // Draw Missiles
                                        foreach (Missile eBlast in guard.eBlasts)
                                        {
                                            eBlast.drawme(spriteBatch, gameTime);
                                        }
                                    }
                                }

                                // Draw Alertbar & backing
                                m_alertBar.drawbacking(spriteBatch);
                                m_alertBar.drawme(spriteBatch);

                                // Draw objective window & display
                                m_objWindow.drawme(spriteBatch);
                                m_objWindow.drawdisplay(spriteBatch);

                                // If player is at a terminal
                                if (m_currentMap.isNewTerminal(m_player1.TilePosition, m_terminalsHacked))
                                {
                                    // Draw objective window help text (terminal)
                                    m_objWindow.drawterminalhelp(spriteBatch);
                                }

                                // If player is at an elevator
                                if ((m_currentMap.isElevator(m_player1.TilePosition)) && (m_currentMap.ElevatorOpen == true))
                                {
                                    // Draw objective window help text (elevator)
                                    m_objWindow.drawelevatorhelp(spriteBatch);
                                }

                                break;
                            }
                        // In the Hack Play State
                        case PlayState.Hack:
                            {
                                // Draw Console & display
                                m_console.drawme(spriteBatch);
                                m_console.drawdisplay(spriteBatch, m_cam.Position);
                                break;
                            }
                        // In the Talk Play State
                        case PlayState.Talk:
                            {
                                // Draw Conversation window & display
                                m_convWindow.drawme(spriteBatch);
                                m_convWindow.drawdisplay(spriteBatch, m_cam.Position);
                                break;
                            }
                        // In the Elevator Playstate
                        case PlayState.Elevator:
                            {
                                // Draw the current floor
                                spriteBatch.DrawString(m_elevatorFont, "Floor: " + m_currentFloor, new Vector2(m_cam.Position.X + 320, m_cam.Position.Y + 260), Color.Red);
                                break;
                            }
                    }
                }
                #endregion
            }
            #endregion

            #region START STATE
            if (m_currentState == GameState.StartState)
            {
                if (m_currentStartScreen != StartScreen.Intro)
                {
                    m_startScreen.drawme(spriteBatch);
                    m_startScreen.drawskyline(spriteBatch);
                }
      
                // Draw the appropriate Start Screen
                switch (m_currentStartScreen)
                {
                    case StartScreen.Menu:
                        {
                            spriteBatch.DrawString(m_convFontSmall, "Press Start to Select", new Vector2(m_cam.Position.X + 60, m_cam.Position.Y + 600), Color.White);
                            // Show start Menu
                            for (int i = 0; i < startMenu.Length; i++)
                            {
                                // If Menu item is selected, show it in cyan
                                if (m_selectedStartMenuItem == i)
                                {
                                    spriteBatch.DrawString(m_convFontSmall, "" + startMenu[i], new Vector2(m_cam.Position.X + 60, m_cam.Position.Y + 620 + i * 10), Color.Cyan);
                                }
                                // Otherwise show it in white
                                else
                                    spriteBatch.DrawString(m_convFontSmall, "" + startMenu[i], new Vector2(m_cam.Position.X + 60, m_cam.Position.Y + 620 + i * 10), Color.White);
                            }
                            break;
                        }
                    case StartScreen.EnterName:
                        {
                            spriteBatch.DrawString(m_convFontSmall, "ENTER NAME:", new Vector2(m_cam.Position.X + 60, +600 + m_cam.Position.Y), Color.White);
                            // Show Name Entry
                            for (int i = 0; i < m_myName.Length; i++)
                                // If Character is selected
                                if (m_selectedChar == i)
                                {
                                    // Show it in cyan
                                    spriteBatch.DrawString(m_convFontSmall, "" + m_myName[i], new Vector2(m_cam.Position.X + 60 + i * 15, +620 + m_cam.Position.Y), Color.Cyan);
                                }
                                // Otherwise show it in white
                                else
                                    spriteBatch.DrawString(m_convFontSmall, "" + m_myName[i], new Vector2(m_cam.Position.X + 60 + i * 15, +620 + m_cam.Position.Y), Color.White);

                            // Show navigation text
                            spriteBatch.DrawString(m_convFontSmall, "Press Start to Confirm, or Back to Return.", new Vector2(m_cam.Position.X + 60, + 640 + m_cam.Position.Y), Color.White);
                            break;
                        }
                    case StartScreen.Intro:
                        {
                            m_console.drawme(spriteBatch);
                            m_console.drawintrodisplay(spriteBatch, m_cam.Position);
                            break;
                        }
                    case StartScreen.HighScores:
                        {
                            // Show HighScore data
                            for (int i = 0; i < currentHighScore.Count; i++)
                            {
                                spriteBatch.DrawString(m_convFontSmall, "HIGH SCORES:", new Vector2(m_cam.Position.X + 60, m_cam.Position.Y + 600), Color.White);
                                spriteBatch.DrawString(m_convFontSmall, "Player: " + currentHighScore.PlayerName[i], new Vector2(m_cam.Position.X + 60, m_cam.Position.Y + 620 + i * 12), Color.White);
                                spriteBatch.DrawString(m_convFontSmall, "Score: " + currentHighScore.Score[i], new Vector2(m_cam.Position.X + 180, m_cam.Position.Y + 620 + i * 12), Color.White);
                            }
                            spriteBatch.DrawString(m_convFontSmall, "Press Back to Return", new Vector2(m_cam.Position.X + 60, m_cam.Position.Y + 680), Color.White);
                            break;
                        }
                    case StartScreen.HowToPlay:
                        {
                            spriteBatch.DrawString(m_convFontSmall, "HOW TO PLAY:", new Vector2(m_cam.Position.X + 60, m_cam.Position.Y + 600), Color.White);
                            spriteBatch.DrawString(m_convFontSmall, "Use the X, Y, A & B Buttons to move.", new Vector2(m_cam.Position.X + 60, m_cam.Position.Y + 620), Color.White);
                            spriteBatch.DrawString(m_convFontSmall, "Use the Left Analog Stick to move the camera.", new Vector2(m_cam.Position.X + 60, m_cam.Position.Y + 632), Color.White);
                            spriteBatch.DrawString(m_convFontSmall, "Don't get caught.", new Vector2(m_cam.Position.X + 60, m_cam.Position.Y + 644), Color.White);
                            spriteBatch.DrawString(m_convFontSmall, "Don't get shot.", new Vector2(m_cam.Position.X + 60, m_cam.Position.Y + 656), Color.White);
                            spriteBatch.DrawString(m_convFontSmall, "Press Back to Return", new Vector2(m_cam.Position.X + 60, m_cam.Position.Y + 688), Color.White);
                            break;
                        }
                    case StartScreen.Credits:
                        {
                            spriteBatch.DrawString(m_convFontSmall, "CREDITS:", new Vector2(m_cam.Position.X + 60, m_cam.Position.Y + 600), Color.White);
                            spriteBatch.DrawString(m_convFontSmall, "Game Design, Development & Programming by Ross McDonald.", new Vector2(m_cam.Position.X + 60, m_cam.Position.Y + 620), Color.White);
                            spriteBatch.DrawString(m_convFontSmall, "Artwork & Music by Ross McDonald.", new Vector2(m_cam.Position.X + 60, m_cam.Position.Y + 632), Color.White);
                            spriteBatch.DrawString(m_convFontSmall, "The following sound effects from https://www.freesound.org:", new Vector2(m_cam.Position.X + 60, m_cam.Position.Y + 644), Color.White);
                            spriteBatch.DrawString(m_convFontSmall, "laser (by Soundslikewillem), doors (by primeval_polypod), alarm & terminal hack (by Timbre).", new Vector2(m_cam.Position.X + 60, m_cam.Position.Y + 656), Color.White);
                            spriteBatch.DrawString(m_convFontSmall, "All other sound by Ross McDonald.", new Vector2(m_cam.Position.X + 60, m_cam.Position.Y + 668), Color.White);
                            spriteBatch.DrawString(m_convFontSmall, "Press Back to Return", new Vector2(m_cam.Position.X + 60, m_cam.Position.Y + 688), Color.White);
                            break;
                        }

 
                }
            }
            #endregion

            #region LOSS STATE
            if (m_currentState == GameState.LossState)
            {
                // Draw the failure message
                m_objWindow.drawlosshelp(spriteBatch, m_failMessage);
            }
            #endregion

            #region WIN STATE
            if (m_currentState == GameState.WinState)
            {
                m_console.drawme(spriteBatch);
                m_console.drawwindisplay(spriteBatch, m_cam.Position);
            }

            #endregion

            #region FADE MASK
            // If doing a fade in or out, draw the fade mask
            if (m_fading == true)
            {
                m_fadeMask.drawmealpha(spriteBatch);
            }
            #endregion

            #region DEBUG TEXT

#if DEBUG
            //// Debugging
            ////PLAYSTATE
            //if (m_currentState == GameState.PlayState)
            //{
            //    spriteBatch.DrawString(debugFont, "Sec Status: " + m_currSecState, new Vector2(m_cam.Position.X, m_cam.Position.Y), Color.White);
            //    spriteBatch.DrawString(debugFont, "Alert: " + m_alertLevel, new Vector2(m_cam.Position.X, m_cam.Position.Y + 10), Color.White);
            //    spriteBatch.DrawString(debugFont, "Hacked: " + m_terminalsHacked.Count, new Vector2(m_cam.Position.X, m_cam.Position.Y + 20), Color.White);
            //    spriteBatch.DrawString(debugFont, "Time: " + m_timeThisLevel, new Vector2(m_cam.Position.X, m_cam.Position.Y + 30), Color.White);
            //    spriteBatch.DrawString(debugFont, "Score: " + m_playerScore, new Vector2(m_cam.Position.X, m_cam.Position.Y + 40), Color.White);

            //    spriteBatch.DrawString(debugFont, "Leftstick: " + m_padCurr.ThumbSticks.Left.X + ", " + m_padCurr.ThumbSticks.Left.Y, new Vector2(m_cam.Position.X, m_cam.Position.Y + 50), Color.White);

            //    // sentry info
            //    for (int i = 0; i < m_currentLevel.Sentries.Count; i++)
            //    {
            //        spriteBatch.DrawString(debugFont, "S:" + i + ", State: " + m_currentLevel.Sentries[i].CurrBehaviour + ", LOS: " + (m_currentLevel.Sentries[i].LOS(m_player1.TilePosition, m_currentMap, m_currSecState)), new Vector2(m_cam.Position.X + 1100, m_cam.Position.Y + (i * 10)), Color.White);
            //        spriteBatch.DrawString(debugFont, "" + i, new Vector2(m_currentLevel.Sentries[i].Position.X, m_currentLevel.Sentries[i].Position.Y), Color.White);
            //    }
            //}

#endif
            #endregion

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
    


