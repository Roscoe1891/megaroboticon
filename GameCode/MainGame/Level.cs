﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace Megaroboticon
{
    /// <summary>
    /// The Level class handles the core elements of a level:
    /// It generates the enemies for the level.
    /// It defines the terminals which must be hacked by the player to progress.
    /// It also handles level events such as the security shutdown procedure, and spawning new enemies.
    /// </summary>
    class Level
    {
        // CLASS VARIABLES

        // List of sentries this level
        List<Enemy> m_sentries;
        public List<Enemy> Sentries
        {
            get
            { return m_sentries; }
        }

        // List of guards this level
        Enemy [] m_guards;
        public Enemy[] Guards
        {
            get
            { return m_guards; }
        }

        // Enemy Textures 
        private Texture2D m_sentrytxr_blue;
        private Texture2D m_sentrytxr_black;
        private Texture2D m_missiletxr;
      
        // Number of terminals required to unlock the elevator
        private int m_terminalsThisFloor;
        public int TerminalsThisFloor
        {
            get
            {
                return m_terminalsThisFloor;
            }
        }

        // Play Level Reference
        private Game1.PlayLevel m_currentPlayLevel;

        // Has the security shutdown proecudure been run?
        private bool m_securityShutdown;
        public bool SecurityShutdown
        {
            get { return m_securityShutdown; }
        }

        // Floor number
        private int m_thisFloor;
        public int ThisFloor
        {
            get { return m_thisFloor; }
        }

        // Level Sound Effects
        private SoundEffect m_shutDown;
        private SoundEffect m_laserSound;

        //CONSTRUCTOR
        public Level(Game1.PlayLevel currentLevel, GameActor player1, Texture2D sentry_txrblue, Texture2D sentry_txrblack, Texture2D missiletxr, SoundEffect lasersound, SoundEffect shutdown)
        {
            // Assign enemy textures
            m_sentrytxr_blue = sentry_txrblue;
            m_sentrytxr_black = sentry_txrblack;
            m_missiletxr = missiletxr;

            // Initialise Enemies
            m_sentries = new List<Enemy>();
            m_guards = new Enemy[12];

            // Assign level sounds
            m_shutDown = shutdown;
            m_laserSound = lasersound;

            // Assign level settings
            m_currentPlayLevel = currentLevel;
            m_securityShutdown = false;
 
            // Check which level is to be constructed
            switch (currentLevel)
            {
                // Level 0: Reset player pos & set terminals only
                case Game1.PlayLevel.LV0:
                    {
                        player1.ResetPosition(new Point(9, 22), Direction.North);
                        m_terminalsThisFloor = 1;
                        m_thisFloor = 0;
                        break;
                    }
                // Level 1: Reset player pos, Spawn sentries & set terminals
                case Game1.PlayLevel.LV1:
                    {
                        player1.ResetPosition(new Point(1, 5), Direction.East);
                        m_terminalsThisFloor = 1;
                        m_thisFloor = 12;
                        #region Spawn Level 1 Sentries
                        m_sentries.Add(new Enemy(new Point(13, 7), new Point(13, 10), sentry_txrblue, missiletxr, lasersound, Direction.South, 12, 12, 3f));
                        m_sentries.Add(new Enemy(new Point(27, 19), new Point(27, 7), sentry_txrblue, missiletxr, lasersound, Direction.North, 12, 12, 3f));
                        m_sentries.Add(new Enemy(new Point(39, 21), new Point(46, 21), sentry_txrblue, missiletxr, lasersound, Direction.West, 12, 12, 3f));
                        m_sentries.Add(new Enemy(new Point(32, 28), new Point(22, 28), sentry_txrblue, missiletxr, lasersound, Direction.East, 12, 12, 3f));
                        #endregion
                        break;
                    }
                // Level 2: Reset player pos, Spawn sentries & set terminals
                case Game1.PlayLevel.LV2:
                    {
                        player1.ResetPosition(new Point(28, 1), Direction.South);
                        m_terminalsThisFloor = 2;
                        m_thisFloor = 40;
                        #region Spawn Level2 Sentries
                        // Batch 1
                        m_sentries.Add(new Enemy(new Point(20, 1), new Point(20, 5), sentry_txrblue, missiletxr, lasersound, Direction.South, 12, 12, 3f));
                        m_sentries.Add(new Enemy(new Point(36, 5), new Point(36, 1), sentry_txrblue, missiletxr, lasersound, Direction.North, 12, 12, 3f));
                        m_sentries.Add(new Enemy(new Point(4, 9), new Point(10, 9), sentry_txrblue, missiletxr, lasersound, Direction.East, 12, 12, 3f));
                        m_sentries.Add(new Enemy(new Point(52, 9), new Point(46, 9), sentry_txrblue, missiletxr, lasersound, Direction.West, 12, 12, 3f));
                       
                        #endregion
                        break;
                    }
                // Level 3: Reset player pos, Spawn sentries & set terminals
                case Game1.PlayLevel.LV3:
                    {
                        player1.ResetPosition(new Point(53, 18), Direction.West);
                        m_terminalsThisFloor = 2;
                        m_thisFloor = 60;
                        #region Spawn Level 3 Sentries & guards

                        // Batch 1
                        m_sentries.Add(new Enemy(new Point(51, 16), new Point(51, 13), sentry_txrblue, missiletxr, lasersound, Direction.North, 12, 12, 3f));
                        m_sentries.Add(new Enemy(new Point(38, 9), new Point(46, 9), sentry_txrblue, missiletxr, lasersound, Direction.East, 12, 12, 3f));
                        m_sentries.Add(new Enemy(new Point(32, 4), new Point(36, 4), sentry_txrblue, missiletxr, lasersound, Direction.East, 12, 12, 3f));
                        m_sentries.Add(new Enemy(new Point(15, 9), new Point(15, 2), sentry_txrblue, missiletxr, lasersound, Direction.North, 12, 12, 3f));
                    
                        // Guards
                        m_guards[0] = new Enemy(new Point(22, 17), new Point(22, 17), sentry_txrblue, missiletxr, lasersound, Direction.West, 12, 12, 3f);
                        m_guards[1] = new Enemy(new Point(22, 18), new Point(22, 18), sentry_txrblue, missiletxr, lasersound, Direction.West, 12, 12, 3f);
                        m_guards[2] = new Enemy(new Point(22, 19), new Point(22, 19), sentry_txrblue, missiletxr, lasersound, Direction.West, 12, 12, 3f);

                        m_guards[3] = new Enemy(new Point(32, 17), new Point(32, 17), sentry_txrblue, missiletxr, lasersound, Direction.East, 12, 12, 3f);
                        m_guards[4] = new Enemy(new Point(32, 18), new Point(32, 18), sentry_txrblue, missiletxr, lasersound, Direction.East, 12, 12, 3f);
                        m_guards[5] = new Enemy(new Point(32, 19), new Point(32, 19), sentry_txrblue, missiletxr, lasersound, Direction.East, 12, 12, 3f);

                        m_guards[6] = new Enemy(new Point(26, 13), new Point(26, 13), sentry_txrblue, missiletxr, lasersound, Direction.North, 12, 12, 3f);
                        m_guards[7] = new Enemy(new Point(27, 13), new Point(27, 13), sentry_txrblue, missiletxr, lasersound, Direction.North, 12, 12, 3f);
                        m_guards[8] = new Enemy(new Point(28, 13), new Point(28, 13), sentry_txrblue, missiletxr, lasersound, Direction.North, 12, 12, 3f);

                        m_guards[9] = new Enemy(new Point(26, 23), new Point(26, 23), sentry_txrblue, missiletxr, lasersound, Direction.South, 12, 12, 3f);
                        m_guards[10] = new Enemy(new Point(27, 23), new Point(27, 23), sentry_txrblue, missiletxr, lasersound, Direction.South, 12, 12, 3f);
                        m_guards[11] = new Enemy(new Point(28, 23), new Point(28, 23), sentry_txrblue, missiletxr, lasersound, Direction.South, 12, 12, 3f);
                        
                        // Set the guards' behaviour
                        setguard();
                       
                        #endregion
                        break;
                    }
                // Level 4: Reset player pos & set terminals only
                case Game1.PlayLevel.LV4:
                    {
                        player1.ResetPosition(new Point(11, 23), Direction.North);
                        m_terminalsThisFloor = 1;
                        m_thisFloor = 75;
                        #region spawn Level 4 sentries
                        m_sentries.Add(new Enemy(new Point(4, 2), new Point(4, 2), sentry_txrblue, missiletxr, lasersound, Direction.East, 12, 12, 3f));
                        m_sentries.Add(new Enemy(new Point(4, 3), new Point(4, 3), sentry_txrblue, missiletxr, lasersound, Direction.East, 12, 12, 3f));
                        m_sentries.Add(new Enemy(new Point(18, 2), new Point(18, 2), sentry_txrblue, missiletxr, lasersound, Direction.West, 12, 12, 3f));
                        m_sentries.Add(new Enemy(new Point(18, 3), new Point(18, 3), sentry_txrblue, missiletxr, lasersound, Direction.West, 12, 12, 3f));

                        m_sentries.Add(new Enemy(new Point(5, 8), new Point(5, 8), sentry_txrblue, missiletxr, lasersound, Direction.East, 12, 12, 3f));
                        m_sentries.Add(new Enemy(new Point(5, 9), new Point(5, 9), sentry_txrblue, missiletxr, lasersound, Direction.East, 12, 12, 3f));
                        m_sentries.Add(new Enemy(new Point(17, 8), new Point(17, 8), sentry_txrblue, missiletxr, lasersound, Direction.West, 12, 12, 3f));
                        m_sentries.Add(new Enemy(new Point(17, 9), new Point(17, 9), sentry_txrblue, missiletxr, lasersound, Direction.West, 12, 12, 3f));

                        m_sentries.Add(new Enemy(new Point(17, 13), new Point(17, 13), sentry_txrblue, missiletxr, lasersound, Direction.West, 12, 12, 3f));
                        m_sentries.Add(new Enemy(new Point(17, 12), new Point(17, 12), sentry_txrblue, missiletxr, lasersound, Direction.West, 12, 12, 3f));
                        m_sentries.Add(new Enemy(new Point(5, 12), new Point(5, 12), sentry_txrblue, missiletxr, lasersound, Direction.East, 12, 12, 3f));
                        m_sentries.Add(new Enemy(new Point(5, 13), new Point(5, 13), sentry_txrblue, missiletxr, lasersound, Direction.East, 12, 12, 3f));

                        m_sentries.Add(new Enemy(new Point(17, 16), new Point(17, 16), sentry_txrblue, missiletxr, lasersound, Direction.West, 12, 12, 3f));
                        m_sentries.Add(new Enemy(new Point(17, 17), new Point(17, 17), sentry_txrblue, missiletxr, lasersound, Direction.West, 12, 12, 3f));
                        m_sentries.Add(new Enemy(new Point(5, 16), new Point(5, 16), sentry_txrblue, missiletxr, lasersound, Direction.East, 12, 12, 3f));
                        m_sentries.Add(new Enemy(new Point(5, 17), new Point(5, 17), sentry_txrblue, missiletxr, lasersound, Direction.East, 12, 12, 3f));
                        #endregion
                        securityshutdown();
                        break;
                    }
            }
        }

        // SET ENEMY TO 'GUARD' BEHAVIOUR
        public void setguard()
        {
            // If playing level 3
            if (m_currentPlayLevel == Game1.PlayLevel.LV3)
            {
                // Set the guards to guard behaviour
                foreach (Enemy guard in Guards)
                {
                    guard.setguard();
                }
            }
            // If playing level 4
            if (m_currentPlayLevel == Game1.PlayLevel.LV4)
            {
                // Deactivate security shutdown and set sentries to guard behaviour
                foreach (Enemy sentry in Sentries)
                {
                    m_securityShutdown = false;
                    sentry.setguard();
                }
            }
        }

        // SPAWN 2ND BATCH SENTRIES
        public void spawnnewsentries(Game1.PlayLevel currentLevel)
        {
            switch(currentLevel)
            {
                // Level 2 Wave 2 Sentries
                case Game1.PlayLevel.LV2:
                    {
                        m_sentries.Clear();
                        m_sentries.Add(new Enemy(new Point(24, 13), new Point(32, 13), m_sentrytxr_blue, m_missiletxr, m_laserSound, Direction.East, 12, 12, 3f));
                        m_sentries.Add(new Enemy(new Point(24, 23), new Point(32, 23), m_sentrytxr_blue, m_missiletxr, m_laserSound, Direction.South, 12, 12, 3f));
                        m_sentries.Add(new Enemy(new Point(37, 31), new Point(41, 31), m_sentrytxr_blue, m_missiletxr, m_laserSound, Direction.East, 12, 12, 3f));
                        m_sentries.Add(new Enemy(new Point(47, 25), new Point(47, 29), m_sentrytxr_blue, m_missiletxr, m_laserSound, Direction.South, 12, 12, 3f));
                        break;
                    }
                // Level 3 Wave 2 Sentries
                case Game1.PlayLevel.LV3:
                    {
                        m_sentries.Clear();
                        m_sentries.Add(new Enemy(new Point(12, 15), new Point(12, 21), m_sentrytxr_blue, m_missiletxr, m_laserSound, Direction.South, 12, 12, 3f));
                        m_sentries.Add(new Enemy(new Point(5, 20), new Point(5, 28), m_sentrytxr_blue, m_missiletxr, m_laserSound, Direction.South, 12, 12, 3f));
                        m_sentries.Add(new Enemy(new Point(20, 36), new Point(20, 32), m_sentrytxr_blue, m_missiletxr, m_laserSound, Direction.North, 12, 12, 3f));
                        m_sentries.Add(new Enemy(new Point(42, 28), new Point(42, 34), m_sentrytxr_blue, m_missiletxr, m_laserSound, Direction.South, 12, 12, 3f));
                        break;
                    }

            }
        }

        // SECURITY SHUTDOWN PROCEDURE
        public void securityshutdown()
        {
                    // Shutdown all sentries
                    foreach (Enemy sentry in Sentries)
                    {
                        sentry.ChangeTextureBlack(m_sentrytxr_black);
                        sentry.securityshutdown();
                    }
                    // On level 3, shutdown all guards
                    if (m_currentPlayLevel == Game1.PlayLevel.LV3)
                    {
                        foreach (Enemy guard in Guards)
                        {
                            guard.ChangeTextureBlack(m_sentrytxr_black);
                            guard.securityshutdown();
                        }
                    }
                    // If not playing level 4 (which begins with the security shutdown)
                    if (m_currentPlayLevel != Game1.PlayLevel.LV4)
                    {
                        // Play the security shutdown sound effect
                        m_shutDown.Play();
                    }
                    // Security Shutdown has been run
                    m_securityShutdown = true;
         }

    }
}
