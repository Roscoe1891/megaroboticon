﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Megaroboticon
{
    /// <summary>
    /// This is the in-game camera, which follows the player (keeping them at the centre of the screen).
    /// The player can use the left analogue stick to move the camera, allowing them to look at more of the map.
    /// This is primarily so that they can identify enemy patrol patterns and plan movement. 
    /// </summary>
    class Camera2D
    {
        // CLASS VARIABLES
        // Position & screen size
        private Vector2 m_position;
        public Vector2 Position
        {
            get { return m_position; }
        }
        private int m_screenWidth, m_screenHeight;

        // Get camera
        public Matrix getCam()
        {
            return Matrix.CreateTranslation(new Vector3(-Position.X, -Position.Y, 0));
        }

        // Adjustment amount defined by position of left analogue stick
        private int m_leftStickAdj;

        // CONSTRUCTOR
        public Camera2D(Vector2 pos, int screenWidth, int screenHeight)
        {
            m_position = pos;
            m_screenWidth = screenWidth;
            m_screenHeight = screenHeight;
            m_leftStickAdj = 3;
        }

        // Reset the camera's position to zero
        public void resetCamera()
        {
            m_position = Vector2.Zero;
        }

        // UPDATE
        public void updateme(Vector2 player1Pos, GamePadState padCurr)
        {
            // Centre position on player, + / - manual adjustment from the left analogue stick
            m_position.X = (player1Pos.X - (m_screenWidth / 2) + padCurr.ThumbSticks.Left.X * m_screenWidth/m_leftStickAdj);
            m_position.Y = (player1Pos.Y - (m_screenHeight / 2) - padCurr.ThumbSticks.Left.Y * m_screenHeight/m_leftStickAdj);
        }
    }
}
