﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;


namespace Megaroboticon
{
    /// <summary>
    /// Parent Class to the PC and Enemy classes.
    /// This defines and handles movement in terms of tiles for the GameActor.
    /// It also handles movement of the Actor's sprite, which is not tethered to tiles to allow smooth movement between tiles.
    /// It contains the common information such as position, texture, and collision rectangle.
    /// It is also used to define whether one Actor has line of sight to another.
    /// It is also used to animate and draw the the Actor's sprite. 
    /// </summary>
    // DIRECTION
    enum Direction
    {
        North, East, South, West
    }

    class GameActor
    {
        // CLASS VARIABLES
        // Actor Position (aligned to tiles)
        private Point m_tilePosition;
        public Point TilePosition
        {
            get
            {
                return m_tilePosition;
            }
        }
        private Vector2 m_tilePosVector2;
        private int m_tileSize;
        
        // Actor Sprite position (not aligned to tiles)
        private Vector2 m_position;
        public Vector2 Position
        {
            get
            {
                return m_position;
            }
        }

        //Collision Rectangle
        private Rectangle m_collisionRect;
        public Rectangle CollisionRect
        {
            get { return m_collisionRect; }
        }
        private const int COLLISIONBORDER = 12;

        // Speed, velocity & facing
        private float m_walkspeed;
        private Vector2 m_velocity;

        private Direction m_facing;
        public Direction Facing
        {
            set
            {
                m_facing = value;
            }
            get
            {
                return m_facing;
            }
        }
  
        // Movement delay
        private int m_moveLagCount;
        public int MoveLagCount
        {
            get
            {
                return m_moveLagCount;
            }
        }

        // Texture
        private Texture2D m_txr;

        // Sprite animation
        private int m_frameCount;
        private int m_animFrame;
        private Rectangle m_sourceRect;
        private float m_updateTrigger;
        private int m_fps;

        // CONSTRUCTOR
        public GameActor(Point startPoint, Texture2D txr, Direction facing, int frameCount, int fps, float walkspeed)
        {
            // Basic position and texture
            m_tilePosition = startPoint;
            m_txr = txr;
            m_tileSize = Game1.TileSize;
            m_position = new Vector2(startPoint.X * m_tileSize, startPoint.Y * m_tileSize);
            m_tilePosVector2 = new Vector2(startPoint.X * m_tileSize, startPoint.Y * m_tileSize);
            m_collisionRect = new Rectangle(((startPoint.X * m_tileSize) + COLLISIONBORDER), ((startPoint.Y * m_tileSize) + COLLISIONBORDER), ((txr.Width / frameCount) - (COLLISIONBORDER * 2)), ((txr.Height / 4) - (COLLISIONBORDER * 2)));

            // Movement
            m_moveLagCount = 0;
            m_walkspeed = walkspeed;
            m_velocity = Vector2.Zero;
            m_facing = facing;

            // Sprite Animation
            m_frameCount = frameCount;
            m_animFrame = 0;
            m_sourceRect = new Rectangle(0, 0, txr.Width / m_frameCount, txr.Height / 4);
            m_updateTrigger = 0;
            m_fps = fps;

        }

        // ACTOR MOVEMENT (aligned to tiles)
        public void moveme(Direction moveDir)
        {
            Facing = moveDir;

            #region DEFINE MOVEMENT BY DIRECTION
            // Movement lag counter ensures a delay in tile-to-tile movement, determined by tile size and actor's speed
            switch (moveDir)
                {
                    // If facing North
                    case Direction.North:
                        // If Move Lag Count is less than the quotient of tile size \ walkspeed
                        if (m_moveLagCount < (m_tileSize / m_walkspeed))
                        {
                            // Increase Move Lag counter
                            m_moveLagCount++;
                            // If Move Lag counter is 1
                            if (m_moveLagCount == 1)
                            {
                                // Move North
                                m_tilePosition.Y--;
                            }
                        }
                        // If Move Lag Count is equal to or more than the quotient of tile size \ walkspeed
                        else
                        {
                            // Reset Move Lag Count
                            m_moveLagCount = 0;
                        }
                        break;

                    // If facing South
                    case Direction.South:
                        // If Move Lag Count is less than the quotient of tile size \ walkspeed
                        if (m_moveLagCount < (m_tileSize / m_walkspeed))
                        {
                            // Increase Move Lag counter
                            m_moveLagCount++;
                            // If Move Lag counter is 1
                            if (m_moveLagCount == 1)
                            {
                                // Move South
                                m_tilePosition.Y++;
                            }
                        }
                        // If Move Lag Count is equal to or more than the quotient of tile size \ walkspeed
                        else
                        {
                            // Reset Move Lag Count
                            m_moveLagCount = 0;
                        }
                        break;

                    // If facing East
                    case Direction.East:
                        // If Move Lag Count is less than the quotient of tile size \ walkspeed
                        if (m_moveLagCount < (m_tileSize / m_walkspeed))
                        {
                            // Increase Move Lag counter
                            m_moveLagCount++;
                            // If Move Lag counter is 1
                            if (m_moveLagCount == 1)
                            {
                                // Move East
                                m_tilePosition.X++;
                            }
                        }
                        // If Move Lag Count is equal to or more than the quotient of tile size \ walkspeed
                        else
                        {
                            // Reset Move Lag Count
                            m_moveLagCount = 0;
                        }
                        break;

                    // If facing West 
                    case Direction.West:
                        // If Move Lag Count is less than the quotient of tile size \ walkspeed
                        if (m_moveLagCount < (m_tileSize / m_walkspeed))
                        {
                            // Increase Move Lag counter
                            m_moveLagCount++;
                            // If Move Lag counter is 1
                            if (m_moveLagCount == 1)
                            {
                                // Move West
                                m_tilePosition.X--;
                            }
                        }
                        // If Move Lag Count is equal to or more than the quotient of tile size \ walkspeed
                        else
                        {
                            // Reset Move Lag Count
                            m_moveLagCount = 0;
                        }
                        break;
                }
            #endregion

            // Update the Tile Position as a Vector2 in pixels
                m_tilePosVector2 = new Vector2(m_tilePosition.X * m_tileSize, m_tilePosition.Y * m_tileSize);
        }

        // PLAYER SPRITE MOVEMENT
        // Input from controller required
        public void moveplayersprite(GamePadState pad_curr)
        {
            // If the player and it's sprite are not in the same position.
            if (m_position != m_tilePosVector2)
            {
                // Calculate the unit vector between the actor's tile position and sprite position.
                m_velocity.X = m_tilePosVector2.X - m_position.X;
                m_velocity.Y = m_tilePosVector2.Y - m_position.Y;
                m_velocity.Normalize();

                //Multiply unit vector by walkspeed
                m_velocity *= m_walkspeed;

                //Add velocity to sprite position
                m_position += m_velocity;

                // If there is no controller input
                if ((pad_curr.Buttons.Y != ButtonState.Pressed) && (pad_curr.Buttons.A != ButtonState.Pressed) && (pad_curr.Buttons.X != ButtonState.Pressed) && (pad_curr.Buttons.B != ButtonState.Pressed))
                {
                    // Increase the move lag count
                    m_moveLagCount++;
                }
            }
            else
            {
                // If the actor and it's sprite are in the same position, reset velocity and lag count
                m_velocity.X = 0;
                m_velocity.Y = 0;
                m_moveLagCount = 0;
           
            }
        }

        // OTHER ACTOR SPRITE MOVEMENT
        // Facing correction required
        public void movesprite()
        {
            // If the actor and it's sprite are not in the same position.
            if (m_position != m_tilePosVector2)
            {

                // Calculate the unit vector between the actor's tile position and sprite position.
                m_velocity.X = m_tilePosVector2.X - m_position.X;
                m_velocity.Y = m_tilePosVector2.Y - m_position.Y;
                m_velocity.Normalize();

                #region CORRECT THE FACING
                //(as the actor's actual position is just slighty ahead of its sprite's)
                // if the x axis is dominant...
                if ((Math.Abs(m_velocity.X)) > (Math.Abs(m_velocity.Y)))
                {
                    // If actor is West of sprite
                    if (m_position.X > m_tilePosVector2.X)
                    {
                        // Face West
                        m_facing = Direction.West;
                    }
                    // If actor is East of sprite
                    else
                    {
                        // Face East
                        m_facing = Direction.East;
                    }
                }
                // If y axis is dominant...
                else
                {
                    // If actor is North of sprite
                    if (m_position.Y > m_tilePosVector2.Y)
                    {
                        // Face North
                        m_facing = Direction.North;
                    }
                    // If actor is South of sprite
                    else
                    {
                        // Face South
                        m_facing = Direction.South;
                    }
                }
                #endregion

                //Multiply unit vector by walkspeed.
                m_velocity *= m_walkspeed;

                //Add velocity to sprite position.
                m_position += m_velocity;
            }
            // If actor and sprite are in the same position, set velocity to 0
            else
                m_velocity = Vector2.Zero;
            
            // Velocity is zero
            if (m_velocity == Vector2.Zero)
            {
                // Reset movement lag count
                m_moveLagCount = 0;
            }
            
        }

        // Move actor's collider
        public void movecollider()
        {
           m_collisionRect.X = (int)m_position.X + COLLISIONBORDER;
           m_collisionRect.Y = (int)m_position.Y + COLLISIONBORDER;
        }

        // LINE OF SIGHT
        public bool LOS(Point target, Map currMap, Game1.SecurityStatus currSecState)
        {
                // Calculation variables
                Point delta, abs, sign, curr;
                int t;

                // Get distance from actor's position to the target
                delta.X = target.X - TilePosition.X;
                delta.Y = target.Y - TilePosition.Y;

                // Get Absolute value of delta (i.e. exclude negative sign)
                abs.X = Math.Abs(delta.X) * 2;
                abs.Y = Math.Abs(delta.Y) * 2;

                // Get the sign (positive or negative) of delta
                sign.X = Math.Sign(delta.X);
                sign.Y = Math.Sign(delta.Y);

                // Calculate whether actor has line of sight
                curr = TilePosition;

                    // Check dominant axis
                    if (abs.X > abs.Y)
                    {

                        // Dominant axis is X
                        t = abs.Y - (abs.X >> 1);
                        do
                        {
                            // Trace vertical line from actor to target
                            if (t >= 0)
                            {
                                curr.Y += sign.Y;
                                t -= abs.X;
                            }

                            curr.X += sign.X;
                            t += abs.Y;
                            // If the actor has traced an unbroken line to the target
                            if ((curr.X == target.X && curr.Y == target.Y))
                            {
                                // If current security status is normal
                                if (currSecState == Game1.SecurityStatus.Normal)
                                {
                                    // actor must be facing the target to see it
                                    if (((m_facing == Direction.North) && (target.Y < TilePosition.Y)) || ((m_facing == Direction.South) && (target.Y > TilePosition.Y)) ||
                                           ((m_facing == Direction.East) && (target.X > TilePosition.X)) || ((m_facing == Direction.West) && (target.X < TilePosition.X)))
                                        return true;
                                    else
                                        return false;
                                }
                                // If current security status isn't normal, the actor can see the target
                                if (currSecState != Game1.SecurityStatus.Normal)
                                    return true;
                            }
                        }
                        while (currMap.isWalkablePlayer(curr));
                        return false;
                    }
                    else
                    {
                        // Dominant axis is Y
                        t = abs.X - (abs.Y >> 1);
                        do
                        {
                            // Trace horizontal line from actor to target
                            if (t >= 0)
                            {
                                curr.X += sign.X;
                                t -= abs.Y;
                            }
                            curr.Y += sign.Y;
                            t += abs.X;
                            // If the actor has traced an unbroken line to the target
                            if ((curr.X == target.X && curr.Y == target.Y))
                            {
                                // If current security status is normal
                                if (currSecState == Game1.SecurityStatus.Normal)
                                {
                                    // actor must be facing the target to see it
                                    if (((m_facing == Direction.North) && (target.Y < TilePosition.Y)) || ((m_facing == Direction.South) && (target.Y > TilePosition.Y)) ||
                                            ((m_facing == Direction.East) && (target.X > TilePosition.X)) || ((m_facing == Direction.West) && (target.X < TilePosition.X)))
                                        return true;
                                    else
                                        return false;
                                }
                                // If current security status isn't normal, the actor can see the target
                                if (currSecState != Game1.SecurityStatus.Normal)
                                    return true;
                            }
                        }
                        while (currMap.isWalkablePlayer(curr));
                        return false;
                    }
                }

        // FACE THE PLAYER
        // Actor is forced to face in the direction of the target (as long as they wouldn't be facing a wall)
        public void faceplayer(GameActor target, Map currMap)
        {
            // if the x axis is dominant...
            if ((Math.Abs(m_tilePosition.X - target.m_tilePosition.X)) > (Math.Abs(m_tilePosition.Y - target.m_tilePosition.Y)))
            {
                // If the target lies to the West, face West
                if (m_tilePosition.X > target.m_tilePosition.X)
                {
                    if (currMap.isWalkablePlayer(new Point(TilePosition.X - 1, TilePosition.Y)))
                    {
                        m_facing = Direction.West;
                    }
                }
                // Otherwise face East
                else
                {
                    if (currMap.isWalkablePlayer(new Point(TilePosition.X + 1, TilePosition.Y)))
                    {
                        m_facing = Direction.East;
                    }
                }
            }
            // if the y axis is dominant...
            else
            {
                // If the target lies to the North, face North
                if (m_tilePosition.Y > target.m_tilePosition.Y)
                {
                    if (currMap.isWalkablePlayer(new Point(TilePosition.X, TilePosition.Y - 1)))
                    {
                        m_facing = Direction.North;
                    }
                }
                // Otherwise face South
                else
                {
                    if (currMap.isWalkablePlayer(new Point(TilePosition.X, TilePosition.Y + 1)))
                    {
                        m_facing = Direction.South;
                    }
                }
            }
        }
        
        // RESET ACTOR POSITION
        public void ResetPosition(Point startPoint, Direction facing)
        {
            m_tilePosition = startPoint;
            m_position = new Vector2(startPoint.X * m_tileSize, startPoint.Y * m_tileSize);
            m_tilePosVector2 = new Vector2(startPoint.X * m_tileSize, startPoint.Y * m_tileSize);
            m_facing = facing;
        }

        // Change actor sprite to red
        public void ChangeTextureRed(Texture2D Redtxr)
        {
            m_txr = Redtxr;
        }

        // Change actor sprite to blue
        public void ChangeTextureBlue(Texture2D Bluetxr)
        {
            m_txr = Bluetxr;
        }

        // Change actor sprite to black
        public void ChangeTextureBlack(Texture2D Blacktxr)
        {
            m_txr = Blacktxr;
        }
        
        // DRAW
        public void drawme(SpriteBatch sb, GameTime gt, Game1.GameState gameState, Game1.PlayState playState)
        {
            // If the actor is moving, use sprite animation
            if ((m_velocity != Vector2.Zero) && (gameState == Game1.GameState.PlayState) && (playState != Game1.PlayState.Hack))
            {
                m_updateTrigger += (float)gt.ElapsedGameTime.TotalSeconds * m_fps;
                // Move the animation window
                if (m_updateTrigger >= 1)
                {
                    m_updateTrigger = 0;

                    m_animFrame = (m_animFrame + 1) % m_frameCount;
                    m_sourceRect.X = m_animFrame * m_sourceRect.Width;
                }
            }
            // Reposition the animation window in accordance with above and current facing
            m_sourceRect.X = m_animFrame * m_sourceRect.Width;
            m_sourceRect.Y = (int)m_facing * m_sourceRect.Height;
          
            // Draw the sprite
            sb.Draw(m_txr, new Vector2(m_position.X, m_position.Y), m_sourceRect, Color.White);
        }
    }
}
