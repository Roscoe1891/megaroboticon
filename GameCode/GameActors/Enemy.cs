﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace Megaroboticon
{
    /// <summary>
    /// The Enemy class is a child of the GameActor class.
    /// Sentries (which appear on all levels 1-4) and guards (which only appear on level 3) use this class.
    /// Enemies follow a set of behaviours for their movement, as follows:
    /// Patrol: The enemy walks back and forth between two points.
    /// Pursue: The enemy walks to the position at which they have 'seen' the player
    /// Wait: On reaching the destination of a pursuit, they will wait.
    /// Return: Assuming they have not begun another pursuit, they will return to their patrol position using a 'crumbtrail' (a list of points they have set down during their pursuit).
    /// Guard: The enemy does not move, and sight of the player does not contribute to raising the alert level.
    /// Shutdown: The enemy does not move or interact with the player in any way.
    /// 
    /// Enemies are also aware of the current 'security status'.
    /// In the 'comprimised' security status they will shoot the player if they have a clear shot.
    /// Each enemy has its own list of missiles, as they only fire one shot at a time.
    /// </summary>
    class Enemy : GameActor
    {
        // Behaviour States
        public enum BehaviourState
        {
            Patrol, Pursue, Wait, Return, Guard, ShutDown
        }

        // CLASS VARIABLES
 
        // Starting and destination points
        private Point m_destPoint;
        private Point m_startPoint;
        private Point m_odestPoint;
        private Point m_ostartPoint;
        List<Point> crumbTrail;

        // Behaviour State
        private BehaviourState m_currBehaviour;
        // (Public version for debugging only)
        public BehaviourState CurrBehaviour
        {
            get
            {
                return m_currBehaviour;
            }
        }
        // Old behaviour state to allow for behaviour state edge detection
        private BehaviourState m_oldBehaviour;

        // Timers (applies to the PATROL & WAIT Behaviour States)
        private int m_patrolWaitTimer;
        private int m_patrolWait;
        private int m_waitTime;
        private int m_waitTimeTimer;

        // Eblast missiles
        private Texture2D m_missileTxr;
        private SoundEffect m_laser;
        public List<Missile> eBlasts;
        int eBlastCharge;

        // CONSTRUCTOR
        public Enemy(Point startPoint, Point destPoint, Texture2D txr, Texture2D missiletxr, SoundEffect laser, Direction facing, int frameCount, int fps, float walkspeed)
            : base(startPoint, txr, facing, frameCount, fps, walkspeed)
        {
            // Starting amd destination points
            m_startPoint = startPoint;
            m_destPoint = destPoint;
            m_ostartPoint = startPoint;
            m_odestPoint = destPoint;
            crumbTrail = new List<Point>();

            // Behaviour
            m_currBehaviour = BehaviourState.Patrol;
            m_oldBehaviour = BehaviourState.Patrol;

            // Timers
            m_patrolWaitTimer = 0;
            m_patrolWait = 60;
            m_waitTimeTimer = 0;
            m_waitTime = 120;
            
            // Eblasts
            m_missileTxr = missiletxr;
            m_laser = laser;
            eBlasts = new List<Missile>();
            eBlastCharge = 20;
        }

        // Update
        public void updateme(GameTime gt, Map currentMap, PC player1, Game1.SecurityStatus secState)
        {
            #region BEHAVIOUR DEPENDENT UPDATES
            switch (m_currBehaviour)
            {
                // Patrol the area between the enemy's original start and destination points
                case BehaviourState.Patrol:
                    {
                        // If the enemy has reached his original destination point 
                        if (TilePosition == m_odestPoint)
                        {
                            // Wait there...
                            m_patrolWaitTimer++;
                            if (m_patrolWaitTimer >= m_patrolWait)
                            {
                                // Then set the original start point as the new destination point
                                m_destPoint = m_ostartPoint;
                                m_patrolWaitTimer = 0;
                            }
                        }
                        // If the enemy has reached his original start point 
                        if (TilePosition == m_ostartPoint)
                        {
                            // Wait there...
                            m_patrolWaitTimer++;
                            if (m_patrolWaitTimer >= m_patrolWait)
                            {
                                // Then set the original destination point as the new destination point
                                m_destPoint = m_odestPoint;
                                m_patrolWaitTimer = 0;
                            }
                        }
                        break;
                    }

                // Pursue the player
                case BehaviourState.Pursue:
                    {
                        // If this is a new pursuit
                        if (m_oldBehaviour != BehaviourState.Pursue)
                        {
                            // Set destination to the position the player was spotted
                            m_startPoint = TilePosition;
                            m_destPoint = player1.TilePosition;
                        }
                        // If enemy reaches the position the player was spotted
                        if (TilePosition == m_destPoint)
                        {
                            // Add this location to the crumbtrail (so he can find his way back)
                            crumbTrail.Add(TilePosition);
                            // Set Behaviour to "Wait"
                            m_waitTimeTimer = 0;
                            m_currBehaviour = BehaviourState.Wait;
                        }
                        // If enemy has pursued the player to a boundary tile
                        if (currentMap.isBoundary(TilePosition))
                        {
                            // Set Behaviour to "Wait"
                            m_waitTimeTimer = 0;
                            m_currBehaviour = BehaviourState.Wait;
                        }
                        break;
                    }

                // Wait in place
                case BehaviourState.Wait:
                    {
                        if (m_currBehaviour == BehaviourState.Wait)
                        {
                            // Wait in current location facing the player
                            m_destPoint = TilePosition;
                            faceplayer(player1, currentMap);

                            // After waiting, set Behaviour State to "Return"
                            m_waitTimeTimer++;
                            if (m_waitTimeTimer > m_waitTime)
                            {
                                m_currBehaviour = BehaviourState.Return;
                            }
                        }
                        break;
                    }

                // Return to original starting point
                case BehaviourState.Return:
                    {
                          // If the are any points in the crumbtrail
                            if (crumbTrail.Count > 0)
                            {
                                // Set destination to the last point in the crumbtrail 
                                m_destPoint = crumbTrail.Last();
                                // When destination point is reached, remove it from the crumbtrail 
                                if (m_destPoint == TilePosition)
                                {
                                    crumbTrail.Remove(crumbTrail.Last());
                                }
                            }
                            // If there are no points in the crumbtrail, return to original destination point
                            else
                            {
                                m_destPoint = m_odestPoint;
                            }
                            // Set behaviour to "Patrol"
                            if (TilePosition == m_odestPoint)
                            {
                                m_destPoint = m_ostartPoint;
                                m_currBehaviour = BehaviourState.Patrol;
                            }
                        break;
                    }
            }

            // Update old Behaviour State
            m_oldBehaviour = m_currBehaviour;
            #endregion
            
            #region BEHAVIOUR EXCLUSIVE UPDATES
            // Excluding Shutdown Behaviour
            if (m_currBehaviour != BehaviourState.ShutDown)
            {
                // And excluding Guard Behaviour
                if ((m_currBehaviour != BehaviourState.Guard))
                {
                    #region MOVEMENT UPDATES
                    // If the enemy is not at his destination point
                    if (TilePosition != m_destPoint)
                    {
                        // if the x axis is dominant...
                        if ((Math.Abs(TilePosition.X - m_destPoint.X)) > (Math.Abs(TilePosition.Y - m_destPoint.Y)))
                        {
                            // Is the destination to the West?
                            if (TilePosition.X > m_destPoint.X)
                            {
                                // If so can the enemy move West?
                                if (currentMap.isWalkableEnemy(new Point(TilePosition.X - 1, TilePosition.Y)))
                                {
                                    moveme(Direction.West);
                                }
                                // If it can't move West does moving North take it closer? 
                                else if (TilePosition.Y > m_destPoint.Y)
                                {
                                    // If so can enemy move North?
                                    if (currentMap.isWalkableEnemy(new Point(TilePosition.X, TilePosition.Y - 1)))
                                    {
                                        moveme(Direction.North);
                                    }
                                }
                                // If so can enemy move South
                                else if (currentMap.isWalkableEnemy(new Point(TilePosition.X, TilePosition.Y + 1)))
                                {
                                    moveme(Direction.South);
                                }
                                // If none of above go East
                                else if (currentMap.isWalkableEnemy(new Point(TilePosition.X + 1, TilePosition.Y)))
                                {
                                    moveme(Direction.East);
                                }
                            }
                            // If the destination is to the East...
                            else
                            {
                                // Can the enemy move East?
                                if (currentMap.isWalkableEnemy(new Point(TilePosition.X + 1, TilePosition.Y)))
                                {
                                    moveme(Direction.East);
                                }
                                // if it can't move East does moving North take it closer? 
                                else if (TilePosition.Y > m_destPoint.Y)
                                {
                                    // If so can enemy move North?
                                    if (currentMap.isWalkableEnemy(new Point(TilePosition.X, TilePosition.Y - 1)))
                                    {
                                        moveme(Direction.North);
                                    }
                                }
                                // If not can the enemy move South?
                                else if (currentMap.isWalkableEnemy(new Point(TilePosition.X, TilePosition.Y + 1)))
                                {
                                    moveme(Direction.South);
                                }
                                // If none of above go West
                                else if (currentMap.isWalkableEnemy(new Point(TilePosition.X - 1, TilePosition.Y)))
                                {
                                    moveme(Direction.West);
                                }
                            }
                        }
                        // If the Y axis is dominant...
                        else
                        {
                            // Is the destination to the North?
                            if (TilePosition.Y > m_destPoint.Y)
                            {
                                // If so can the enemy move North?
                                if (currentMap.isWalkableEnemy(new Point(TilePosition.X, TilePosition.Y - 1)))
                                {
                                    moveme(Direction.North);
                                }
                                // If the enemy can't move North does moving West take it closer?
                                else if (TilePosition.X > m_destPoint.X)
                                {
                                    if (currentMap.isWalkableEnemy(new Point(TilePosition.X - 1, TilePosition.Y)))
                                    {
                                        moveme(Direction.West);
                                    }
                                }
                                // If not can it move East? 
                                else if (currentMap.isWalkableEnemy(new Point(TilePosition.X + 1, TilePosition.Y)))
                                {
                                    moveme(Direction.East);
                                }
                                // If none of above go South
                                else if (currentMap.isWalkableEnemy(new Point(TilePosition.X, TilePosition.Y + 1)))
                                {
                                    moveme(Direction.South);
                                }
                            }
                            // If the destination is to the South...
                            else
                            {
                                // Can the enemy move South?
                                if (currentMap.isWalkableEnemy(new Point(TilePosition.X, TilePosition.Y + 1)))
                                {
                                    moveme(Direction.South);
                                }
                                // If the enemy can't move South does moving West take it closer?
                                else if (TilePosition.X > m_destPoint.X)
                                {
                                    if (currentMap.isWalkableEnemy(new Point(TilePosition.X - 1, TilePosition.Y)))
                                    {
                                        moveme(Direction.West);
                                    }
                                }
                                // If not can the enemy move East?
                                else if (currentMap.isWalkableEnemy(new Point(TilePosition.X + 1, TilePosition.Y)))
                                {
                                    moveme(Direction.East);
                                }
                                // If none of the above move North
                                else if (currentMap.isWalkableEnemy(new Point(TilePosition.X, TilePosition.Y - 1)))
                                {
                                    moveme(Direction.North);
                                }
                            }
                        }
                    }
                    #endregion

                    // Change behaviour to Pursue on spotting the player
                    if (LOS(player1.TilePosition, currentMap, secState))
                    {
                        m_currBehaviour = BehaviourState.Pursue;
                    }
                }
                // IN THE COMPROMISED SECURITY STATE
                if (secState == Game1.SecurityStatus.Comprimised)
                {
                    // FIRING EBLASTS
                    // If the enemy can see the player and has a clear shot
                    if ((LOS(player1.TilePosition, currentMap, secState)) && (TilePosition.X == player1.TilePosition.X || TilePosition.Y == player1.TilePosition.Y))
                    {
                        // Face the player
                        faceplayer(player1, currentMap);

                            // Shoot an energy blast
                            if (eBlasts.Count < 1)
                            {
                                eBlastCharge--;
                                if (eBlastCharge <= 0)
                                {
                                    eBlasts.Add(new Missile(TilePosition, Facing, m_missileTxr));
                                    m_laser.Play();
                                    eBlastCharge = 20;
                                }
                            }
                        
                    }
                }
            }
            #endregion

            #region COMMON UPDATES
            // Update eblasts
            foreach (Missile eBlast in eBlasts)
            {
                eBlast.updateme(gt, currentMap, player1);
                eBlast.movesprite();
                eBlast.movecollider();
            }

            // Remove any eblasts that have hit a wall
            for (int i = 0; i < eBlasts.Count; i++)
            {
                if (eBlasts[i].Hitsomething == true)
                {
                    eBlasts.RemoveAt(i);
                }
            }

            // Move the enemy's sprite & collider to follow actual position.
            movesprite();
            movecollider();
            #endregion
        }

        // Set behaviour state to Guard
        public void setguard()
        {
            m_currBehaviour = BehaviourState.Guard;
        }

        // Set behaviour state to Shutdown
        public void securityshutdown()
        {
            m_currBehaviour = BehaviourState.ShutDown;
        }
           
        }


       

    
}
