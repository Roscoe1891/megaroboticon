﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Megaroboticon
{
    /// <summary>
    /// A child of the GameActor class.
    /// This is used to detect input from the controller, which prompts movement (movement itself handled by the parent class).
    /// </summary>
    
    class PC : GameActor
    {
        // CONSTRUCTOR
        public PC(Point startPoint, Texture2D txr, Direction facing, int frameCount, int fps, float walkspeed)
            : base(startPoint, txr, facing, frameCount, fps, walkspeed)
        {
        }

        // UPDATE
        public void updateme(GameTime gameTime, Map currentMap, GamePadState pad_curr, GamePadState pad_old)
        {
            {
                // Check input for Northward movement.
                if ((pad_curr.Buttons.Y == ButtonState.Pressed) && (pad_curr.Buttons.X != ButtonState.Pressed) && (pad_curr.Buttons.A != ButtonState.Pressed) && (pad_curr.Buttons.B != ButtonState.Pressed))
                {
                    // If North is walkable, move North
                    if (currentMap.isWalkablePlayer(new Point(TilePosition.X, TilePosition.Y - 1)))
                    {
                        moveme(Direction.North);
                    }
                }

                // Check input for Southward movement.
                if ((pad_curr.Buttons.A == ButtonState.Pressed) && (pad_curr.Buttons.X != ButtonState.Pressed) && (pad_curr.Buttons.B != ButtonState.Pressed) && (pad_curr.Buttons.Y != ButtonState.Pressed))
                {
                    // If South is walkable, move South
                    if (currentMap.isWalkablePlayer(new Point(TilePosition.X, TilePosition.Y + 1)))
                    {
                        moveme(Direction.South);
                    }
                }

                // Check input for Western movement.
                if ((pad_curr.Buttons.X == ButtonState.Pressed) && (pad_curr.Buttons.A != ButtonState.Pressed) && (pad_curr.Buttons.B != ButtonState.Pressed) && (pad_curr.Buttons.Y != ButtonState.Pressed))
                {
                    // If West is walkable, move West
                    if (currentMap.isWalkablePlayer(new Point(TilePosition.X - 1, TilePosition.Y)))
                    {
                        moveme(Direction.West);
                    }
                }

                // Check input for Eastern movement.
                if ((pad_curr.Buttons.B == ButtonState.Pressed) && (pad_curr.Buttons.Y != ButtonState.Pressed) && (pad_curr.Buttons.X != ButtonState.Pressed) && (pad_curr.Buttons.A != ButtonState.Pressed))
                {
                    // If East is walkable, move East
                    if (currentMap.isWalkablePlayer(new Point(TilePosition.X + 1, TilePosition.Y)))
                    {
                        moveme(Direction.East);
                    }
                }

                // Move the sprite & collider to follow.
                moveplayersprite(pad_curr);
                movecollider();
            }
        }       
    }
}
