﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Megaroboticon
{
    /// <summary>
    /// The Missile class is at present only used by Enemies to fire energy blasts.
    /// Energy blasts are game objects instantiated by Enemies.
    /// They move at a contant speed in the direction they 'fired' and are removed by Game1 if they have reached a non walkable tile.
    /// Game1 also handles their collision with the player, which ends the current level attempt.
    /// </summary>
    
    class Missile
    {
        // Class Variables

        // Position & Facing
        private Point m_startPoint;
        private Direction m_facing;
        private Point m_tilePosition;
        public Point TilePosition
        {
            get { return m_tilePosition; }
        }
        private Vector2 m_tilePosVector2;
        private Vector2 m_position;
        

        // Collision Rectangle 
        private Rectangle m_collisionRect;
        public Rectangle CollisionRect
        {
            get { return m_collisionRect; }
        }
        private const int COLLISIONBORDER = 0;

        // Movement & Texture
        private Texture2D m_txr;
        private Vector2 m_velocity;
        private int m_moveLagCount;
        private float m_speed;
        private bool m_hitSomething;
        public bool Hitsomething
        {
            get { return m_hitSomething; }
        }

        // CONSTRUCTOR
        public Missile(Point startPoint, Direction facing, Texture2D txr)
        {
            // Position & facing
            m_startPoint = startPoint;
            m_facing = facing;
            m_tilePosition = startPoint;
            m_tilePosVector2 = new Vector2((m_tilePosition.X * Game1.TileSize) + 20, (m_tilePosition.Y * Game1.TileSize + 20));
            m_position = m_tilePosVector2;    
            
            // Movement & texture
            m_moveLagCount = 0;
            m_speed = 40;
            m_txr = txr;

            // Collision
            m_hitSomething = false;
            m_collisionRect = new Rectangle(((startPoint.X * Game1.TileSize) + 20 + COLLISIONBORDER), ((startPoint.Y * Game1.TileSize) + 20 + COLLISIONBORDER),((txr.Width) - (COLLISIONBORDER * 2)), ((txr.Height / 4) - (COLLISIONBORDER * 2)));
        }

        // Update
        public void updateme(GameTime gt, Map currentMap, PC player1)
        {
            #region UPDATE MOVEMENT
            // DEFINE MOVEMENT BY DIRECTION
            switch (m_facing)
            {
                // If facing West
                case Direction.West:
                    {
                        //Can it move West?
                        if (currentMap.isWalkablePlayer(new Point(m_tilePosition.X - 1, m_tilePosition.Y)))
                        {
                            // If move lag count is less than the quotient of tile size / missile speed
                            if (m_moveLagCount < (Game1.TileSize / m_speed))
                            {
                                // Increase Move Lag Count
                                m_moveLagCount++;
                                // If Move Lag count is 1
                                if (m_moveLagCount == 1)
                                {
                                    // Move West
                                    m_tilePosition.X--;
                                }
                            }
                            // If move lag count is equal to or more than the quotient of tile size / missile speed
                            else
                            {
                                // Reset Move Lag Count
                                m_moveLagCount = 0;
                            }

                        }
                        // If it can't move, it has hit soemthing
                        else m_hitSomething = true;
                        break;
                    }
                case Direction.East:
                    {
                        // Can it move East?
                        if (currentMap.isWalkablePlayer(new Point(m_tilePosition.X + 1, m_tilePosition.Y)))
                        {
                            // If move lag count is less than the quotient of tile size / missile speed
                            if (m_moveLagCount < (Game1.TileSize / m_speed))
                            {
                                // Increase Move Lag Count
                                m_moveLagCount++;
                                // If Move Lag count is 1
                                if (m_moveLagCount == 1)
                                {
                                    // Move East
                                    m_tilePosition.X++;
                                }
                            }
                            // If move lag count is equal to or more than the quotient of tile size / missile speed
                            else
                            {
                                // Reset Move Lag Count
                                m_moveLagCount = 0;
                            }
                        }
                        // If it can't move, it has hit soemthing
                        else m_hitSomething = true;
                        break;
                    }
                case Direction.North:
                    {
                        // Can it move North?
                        if (currentMap.isWalkablePlayer(new Point(m_tilePosition.X, m_tilePosition.Y - 1)))
                        {
                            // If move lag count is less than the quotient of tile size / missile speed
                            if (m_moveLagCount < (Game1.TileSize / m_speed))
                            {
                                // Increase Move Lag Count
                                m_moveLagCount++;
                                // If Move Lag count is 1
                                if (m_moveLagCount == 1)
                                {
                                    // Move North
                                    m_tilePosition.Y--;
                                }
                            }
                            // If move lag count is equal to or more than the quotient of tile size / missile speed
                            else
                            {
                                // Reset Move Lag Count
                                m_moveLagCount = 0;
                            }
                        }
                        // If it can't move, it has hit something
                        else m_hitSomething = true;
                        break;
                    }
                case Direction.South:
                    {
                        // Can it move South?
                        if (currentMap.isWalkablePlayer(new Point(m_tilePosition.X, m_tilePosition.Y + 1)))
                        {
                            // If move lag count is less than the quotient of tile size / missile speed
                            if (m_moveLagCount < (Game1.TileSize / m_speed))
                            {
                                // Increase Move Lag Count
                                m_moveLagCount++;
                                // If Move Lag count is 1
                                if (m_moveLagCount == 1)
                                {
                                    // Move South
                                    m_tilePosition.Y++;
                                }
                            }
                            // If move lag count is equal to or more than the quotient of tile size / missile speed
                            else
                            {
                                // Reset Move Lag Count
                                m_moveLagCount = 0;
                            }
                        }
                        // If it can't move, it has hit something
                        else m_hitSomething = true;
                        break;
                    }
            }
            #endregion

            // Update the Tile Position as a Vector2 in pixels
            m_tilePosVector2 = new Vector2((m_tilePosition.X * Game1.TileSize + 20), (m_tilePosition.Y * Game1.TileSize + 20));
        }
       
        // MISSILE SPRITE MOVEMENT  
        public void movesprite()
        {
            // If the actor and it's sprite are not in the same position.
            if (m_position != m_tilePosVector2)
            {
                // Calculate the unit vector between the actor's tile position and sprite position.
                m_velocity.X = m_tilePosVector2.X - m_position.X;
                m_velocity.Y = m_tilePosVector2.Y - m_position.Y;
                m_velocity.Normalize();

                //Multiply unit vector by speed.
                m_velocity *= m_speed;

                //Add velocity to sprite position.
                m_position += m_velocity;
            }
        }

        // MOVE MISSILE COLLIDER
        public void movecollider()
        {
            // Update position of collision rectangle
            m_collisionRect.X = (int)m_position.X + COLLISIONBORDER;
            m_collisionRect.Y = (int)m_position.Y + COLLISIONBORDER;
        }

        // DRAW MISSILE
        public void drawme(SpriteBatch sb, GameTime gt)
        {
            sb.Draw(m_txr, m_position, Color.White);
        }

     }
}

